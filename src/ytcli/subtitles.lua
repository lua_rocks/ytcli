-- 23-02-2024 @author Swarg
--

local M = {}
--

--[==[
<?xml version="1.0" encoding="utf-8" ?>
<transcript>
<text start="22.8" dur="1.94">
What&amp;#39;s up?
Welcome to Backend Stuff.
</text>
<text start="1037.32" dur="1.48">
See you next time, guys. Bye.
</text></transcript>
]==]
-- https://www.dvteclipse.com/documentation/svlinter/How_to_use_special_characters_in_XML.3F.html#gsc.tab=0


local gsub = string.gsub
local entityMap = { ["lt"] = "<", ["gt"] = ">", ["amp"] = "&", ["quot"] = '"', ["apos"] = "'" }
local entitySwap = function(orig, n, s)
  return (n == '' and entityMap[s])
      or (n == "#" and tonumber(s)) and string.char(s)
      or (n == "#x" and tonumber(s, 16)) and string.char(tonumber(s, 16))
      or orig
end

---@param str string
function M.xml_unescape(str)
  return (gsub(str, '(&(#?x?)([%d%a]+);)', entitySwap))
end

--[[
WEBVTT - Subtitles by: DownloadYoutubeSubtitles.com

00:00:22.800 --> 00:00:24.740
What's up?
Welcome to Backend Stuff.

00:00:24.760 --> 00:00:27.460
This is episode 2 in Elixir Basics.

00:00:27.480 --> 00:00:29.500
Before we can dive into writing any code,
]]

---
---@param sub table{_attr:table{start, dur} [0]:string}
---@param nextsub table?
---@return string
local function build_vtt_sub(sub, nextsub)
  local text = sub[1] ~= nil and M.xml_unescape(sub[1]) or ''
  local start = assert(sub._attr.start, 'has attr start')
  local tend = tonumber(start) + tonumber(sub._attr.dur)
  -- fix wrong dur
  -- local nstart = nextsub ~= nil and nextsub._attr.start or tend
  -- print('@@@', tend, (nextsub or {})._attr.start)
  -- print('@@@', type(tend), type((nextsub or {})._attr.start))
  if nextsub then
    local nstart = tonumber(nextsub._attr.start) or tend
    if tend > nstart then
      tend = nstart - 0.1
    end
  end
  local time = M.fmt_sec(start) .. ' --> ' .. M.fmt_sec(tend)
  return time .. "\n" .. text .. "\n\n"
end

--
-- raw xml -> WEBVTT (.vtt)
--
---@param xml string xml-body from yt to convert
---@param lang string?
---@diagnostic disable-next-line: unused-local
function M.convert_xml_subs_to_vtt(xml, lang)
  local xml2lua = require("xml2lua")
  local handler = require("xmlhandler.tree")

  local parser = xml2lua.parser(handler)
  parser:parse(xml)
  local s = "WEBVTT\n\n"
  -- Kind: captions
  -- Language: en                              << TODO

  -- start:	22.8	dur:	1.94
  local prev = nil

  local transcript = handler.root.transcript
  if not transcript then error('no transcript') end
  if not transcript.text then error('no transcript.text') end

  for _, curr_sub in pairs(transcript.text) do
    if prev ~= nil then
      s = s .. build_vtt_sub(prev, curr_sub)
    end
    prev = curr_sub
  end

  s = s .. build_vtt_sub(prev, nil)

  handler.root.transcript = nil -- cleanup
  return s
end

--
-- 22.8 -> '00:00:22.800'
--
---@param sec number of secords 22.8
function M.fmt_sec(sec)
  local s = math.floor(sec)
  local ms = (sec - s) * 1000
  local time = string.format("%.2d:%.2d:%.2d.%.3d",
    s / (60 * 60), s / 60 % 60, s % 60, ms)
  return time
end

return M
