-- 22-02-2024 @author Swarg
--
local log = require 'alogger'
local base = require 'cli-app-base'

local zlib = require 'zlib' -- for gzip  luarocks lua-zlib
local cjson = require 'cjson'
local http = require("socket.http")
local ltn12 = require("ltn12")
local inspect = require 'inspect'

local M = {}
---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

--
local api_v1 = "https://www.youtube.com/youtubei/v1/"
local API_KEY_V1 = "AIzaSyDCU8hByM-4DrUqRUYnGn-3llEO78bcxq8"

local api_v3 = 'https://youtube.googleapis.com/youtube/v3/'

M.VID_LEN = 11
M.PLID_LEN = 34
--

local post_query_v1 = [[{
  "context": {
    "client": {
      "hl": "en",
      "clientName": "WEB",
      "clientVersion": "2.20210721.00.00",
      "clientFormFactor": "UNKNOWN_FORM_FACTOR",
      "clientScreen": "WATCH",
    },
    "user": {
      "lockedSafetyMode": false
    },
    "request": {
      "useSsl": true,
      "internalExperimentFlags": [],
      "consistencyTokenJars": []
    }
  },
  "playbackContext": {
    "contentPlaybackContext": {
      "vis": 0,
      "splay": false,
      "autoCaptionsDefaultOn": false,
      "autonavState": "STATE_NONE",
      "html5Preference": "HTML5_PREF_WANTS",
      "lactMilliseconds": "-1"
    }
  },
  "racyCheckOk": false,
  "contentCheckOk": false,
  ${DATA}
}
]]


---@param url string
---@param post_body string
---@param accept string?
function M.send_post(url, post_body, accept)
  log.debug("send_post")
  assert(type(url) == 'string', 'url')

  local response_body = {}

  local source = ltn12.source.string(post_body);

  local new_reqt = {
    scheme = "https",
    method = 'POST',
    url = url,
    headers = {
      accept = accept or 'application/json',
      --
      ['content-type'] = 'application/json',
      ['content-length'] = tostring(#post_body),
      ['accept-encoding'] = "gzip",
    },
    sink = ltn12.sink.table(response_body),
    source = source
  }
  log.debug('POST', new_reqt.url)
  local r, code, headers, status = http.request(new_reqt)
  if not r then
    log.debug('http.req err:' .. tostring(code))
    return false
  end

  if code ~= 200 then
    print('[DEBUG]', inspect(new_reqt))
    print('[DEBUG]', post_body)
    print('[DEBUG] code:', code, status)
    log.debug('response code is', code, status)
    return false
  end

  -- parse json response
  local content_type = (headers or E)['content-type']
  if not content_type or accept and not string.find(content_type, accept) then
    log.debug('Not Supported Content-Type:%s expected:%s ', content_type, accept)
    return false
  end

  local resp_body = table.concat(response_body)

  if (headers or E)["content-encoding"] == "gzip" then
    log.debug('ungzip content')
    local inflated, eof, bytes_in, bytes_out = zlib.inflate()(resp_body)
    resp_body = eof == true and inflated or '' -- ?
    log.debug('ungziped:', eof, bytes_in, bytes_out)
  end

  log.debug("resp-body len:", #resp_body)

  return true, resp_body
end

--
-- download GET responce for given url
--
---@param url string
---@param expected_content string? -- Accept
---@param proxy string?
---@return boolean
---@return string? respbody
function M.send_get(url, expected_content, proxy)
  log.debug("get_http_content")
  assert(type(url) == 'string', 'url')
  -- expected_content = expected_content or 'text/xml; charset=UTF-8'

  local response_body = {}
  local new_reqt = {
    scheme = "https",
    method = 'GET',
    url = url,
    headers = {
      accept = expected_content or 'application/json',
      ['accept-encoding'] = "gzip",
    },
    sink = ltn12.sink.table(response_body),
    -- proxy = proxy, give error
  }
  if proxy and proxy ~= '' then
    if not string.find(proxy, '://') then
      proxy = 'https://' .. proxy
    end
    print('[DEBUG] via proxy:', proxy)
    -- http.PROXY = proxy -- global scope
    new_reqt.proxy = proxy
  end

  local r, code, headers, status = http.request(new_reqt)
  if not r then
    log.debug('http.req err:' .. tostring(code))
    return false
  end

  if code ~= 200 then
    print('[DEBUG]', inspect(new_reqt))
    print('[DEBUG] code:', code, status)
    log.debug('response code is', code, status)
    return false
  end

  -- parse json response
  local content_type = (headers or E)['content-type']
  if expected_content then
    if not content_type or not string.find(content_type, expected_content) then
      log.debug('Not Supported Content-Type: ' .. tostring(content_type))
      return false
    end
  end

  local respbody = table.concat(response_body)

  if (headers or E)["content-encoding"] == "gzip" then
    log.debug('ungzip content')
    local inflated, eof, bytes_in, bytes_out = zlib.inflate()(respbody)
    respbody = eof == true and inflated or '' -- ?
    log.debug('ungziped:', eof, bytes_in, bytes_out)
  end

  log.debug("resp body len:", #respbody)

  return true, respbody
end

--
---@param filename string
---@return string  inflated
---@return boolean eof
---@return number  bytes_in
---@return number  bytes_out
function M.ungzip(filename)
  local bin = base.read_all(filename)

  local stream = zlib.inflate()
  local inflated, eof, bytes_in, bytes_out = stream(bin)

  return inflated, eof, bytes_in, bytes_out
end

--
--
--
---@param str_json string
function M.decode_json(str_json)
  assert(type(str_json) == 'string', 'raw_json')
  local ok, decoded = pcall(cjson.decode, str_json)
  if not ok then
    local err_msg = decoded
    log.debug('Cannot parse json responce: %s', err_msg)
    return nil
  end
  return decoded -- table
end

--
-- via api v1
--
---@param vid string
function M.get_video_info(vid)
  log.debug("get_video_info")
  assert(type(vid) == 'string', 'vid')

  local url = api_v1 .. 'player?key=' .. API_KEY_V1 -- can work without api key!
  local data = ' "videoId": "' .. vid .. '"'
  local post_body = string.gsub(post_query_v1, "${DATA}", data)

  local st, resp_body = M.send_post(url, post_body, 'application/json')

  return st, resp_body
end

-- jq .captions.playerCaptionsTracklistRenderer.captionTracks file.json
-- captions.playerCaptionsTracklistRenderer.captionTracks

--
-- subtitles for one video
--
---@param raw_json string
---@return table?
---@return string? errmsg
function M.get_subtitles_list(raw_json, verbose)
  log.debug("get_subtitles_list")
  if raw_json == nil or raw_json == '' then
    log.debug('expected raw json got:%s', raw_json)
    return nil
  end
  local t = M.decode_json(raw_json)
  if not t then return nil end

  local t0
  t0 = ((t.captions or E).playerCaptionsTracklistRenderer or E).captionTracks

  if not t0 then
    local status = (t.playabilityStatus or E).status
    if status ~= nil then
      return nil, status .. ':' .. v2s(t.playabilityStatus.reason)
    end
    base.save_dump_on('YTCLI_DUMP', t, 'all decoded json response')
    log.debug('cannot find path in the json')
    return nil
  end

  -- if not verbose then
  local subs = {}
  for _, el in pairs(t0) do
    local name = tostring((el.name or E).simpleText)
    subs[#subs + 1] = { name = name, url = el.baseUrl, kind = el.kind }
    if verbose then
      print(inspect(el))
    end
  end

  table.sort(subs, function(a, b)
    return a.name < b.name
  end)

  log.debug('found subtitles:', #subs)
  return subs
end

--
--
--
---@param subtitles table?{name: url}
function M.fotmat_subtitles(subtitles, verbose)
  local s = ''
  if subtitles then
    for i, v in pairs(subtitles) do
      if #s > 0 then s = s .. "\n" end
      s = s .. string.format("%2s %s", i, tostring(v.name))
      if verbose then
        s = s .. ' ' .. tostring(v.kind) .. ' ' .. tostring(v.url)
      end
    end
  end
  return s
end

--
-- via api v1
-- short to get total video counts in given playlist
--
function M.get_playlist_info(plid)
  log.debug("get_palylist_info")
  assert(type(plid) == 'string', 'vid')

  local url = api_v1 .. 'browse?key=' .. API_KEY_V1 -- can work without api-key
  -- local post_body = string.gsub(post_query_playlist, "${PLID}", plid)

  local data = '  "browse_id": "VL' .. plid .. '"'
  local post_body = string.gsub(post_query_v1, "${DATA}", data)
  local st, resp_body = M.send_post(url, post_body, 'application/json')
  return st, resp_body
end

--
--
--
---@param cid string
---@param apikey_v3 string
function M.get_channel_info(cid, apikey_v3)
  assert(type(apikey_v3) == 'string', 'apikey_v3')

  local url = api_v1 .. 'browse?' ..
      'part=' ..
      'contentDetails%2Csnippet' .. -- statistics
      -- '&maxResults=50'..
      '&id=' .. cid .. '&key=' .. apikey_v3

  local data = '  "browse_id": "' .. cid .. '"'
  local post_body = string.gsub(post_query_v1, "${DATA}", data)
  return M.send_post(url, post_body)
end

--[[
https://www.googleapis.com/youtube/v3/videos?key=[YOUR_API_KEY_HERE]&fields=items(snippet(title,tags,channelTitle,publishedAt),statistics(viewCount))&part=snippet,statistics&id=[VIDEOID]

GET https://www.googleapis.com/youtube/v3/videos?part=statistics&id=<video-id>&key={YOUR_API_KEY}
]]

--
-- via api-v3 and API_KEY_V3
-- issue: get total viewCount for given playlist
--
---@param plid string
---@param apikey_v3 string
function M.get_playlist_info_v3(plid, apikey_v3)
  assert(type(apikey_v3) == 'string', 'apikey_v3')

  -- local url = api_v3 .. 'playlistItems?&playlistId=' ..
  local url = api_v3 .. 'playlists?key=' .. apikey_v3 ..
      '&part=' ..
      'contentDetails%2Csnippet' .. -- statistics- status+ ..
      -- '&maxResults=50'..
      '&id=' ..
      plid
  return M.send_get(url)
end

--
-- videos count in the json from api-v3 of the playlist
--
---@param str_json string
function M.get_playlist_item_count(str_json)
  local t = M.decode_json(str_json)
  return ((((t or E).items or E)[1] or E).contentDetails or E).itemCount
end

--
-- vid title
-- views publishedtime
-- description
-- keywords
--
---@param json string
function M.get_readable_video_info(json)
  local t = M.decode_json(json)
  local vd = ((t or E).videoDetails or E)
  local m = ((t or E).microformat or E)
  if not vd then return end
  local s = ''
  local ts = tostring
  s = ts(vd.videoId) .. '  ' .. ts(vd.title) .. "\n" ..
      ts(vd.viewCount) .. ' views'
  local date = (m.playerMicroformatRenderer or E).publishDate
  if date then
    s = s .. ' ' .. date:sub(1, 16) -- 2023-10-29T22:00:22-07:00
  end

  local desc = (vd).shortDescription or (m.description or E).simpleText
  if desc then
    s = s .. "\n" .. desc:gsub('\n', "\n")
  end
  s = s .. "\n"

  if vd.keywords then
    local kw, len = '', 0
    for _, v in ipairs(vd.keywords) do
      local sep = ' '
      if len + #v > 80 then
        len, sep = 0, "\n"
      else
        len = len + #v
      end
      if #kw > 0 then kw = kw .. ',' .. sep end
      kw = kw .. v
    end
    s = s .. "\nkeywords: " .. kw .. "\n"
  end
  return s
end

--
-- Title_<videoId>-c32_ab48k.mp4  -->  <videoId>
--
---@param fn string
function M.get_vid_from_filename(fn)
  local vid = fn:sub(-11, -1)
  --._c32_ab48k
  if string.match(vid, "^._c%d%d_ab%d+k$") then
    vid = fn:sub(-21, -11)
  end
  -- print('##|' .. tostring(vid) .. '|##')
  return vid
end

--
-- extract unique videoids from all filenames in the given path
--
---@param path string
function M.get_vids_from_path(path)
  local list = base.execrl('ls ' .. (path or '.'))
  local t = {} -- vid:index
  if list then
    for i, name in pairs(list) do
      if #name >= 11 then
        local vid = M.get_vid_from_filename(base.filename_without_extension(name))
        if vid and vid ~= '' then
          t[vid] = i
        end
      end
    end
  end
  local vids = {}
  for k, _ in pairs(t) do
    vids[#vids + 1] = k
  end
  return vids
end

--------------------------------------------------------------------------------

--
-- parse responce from v1 to get all videoId, index, title, length, isPlayable
--
---@param json string
---@return boolean
---@return table|string
function M.get_playlist_content(json)
  if not json then return false, 'no json' end
  local data = M.decode_json(json)
  if not data then
    return false, 'cannot decode json'
  end

  local path = { 'contents', 'twoColumnBrowseResultsRenderer', 'tabs', 1,
    'tabRenderer', 'content', 'sectionListRenderer', 'contents', 1,
    "itemSectionRenderer", 'contents', 1,
    "playlistVideoListRenderer", "contents"
  }
  local ok, tabs = base.tbl_get_node(data, path)
  if not ok then return false, tabs end -- errmsg

  local t = {}

  for _, e in pairs(tabs) do
    e = (e or E).playlistVideoRenderer
    if type(e) == 'table' then
      local short_info = M.mk_playlist_item_data(e)
      t[#t + 1] = short_info
    end
  end

  return true, t
end

function M.mk_playlist_item_data(e)
  return {
    vid = e.videoId,
    title = (((e.title or E).runs or E)[1] or E).text,
    index = (e.index or E).simpleText,
    length = (e.lengthText or E).simpleText,
    isPlayable = e.isPlayable
  }
end

--------------------------------------------------------------------------------

--
-- show playlist title + video-items
--
---@param apikey string
---@param id string playlist-id
---@param json string? (for testing response from api)
function M.subscription_check_playlist(apikey, id, json)
  local st, body = json ~= nil, json
  if not json then
    st, body = M.get_playlist_info_v3(id, apikey)
  end

  local out = {}
  if st and body then
    local t = M.decode_json(body)
    if t and t.kind == "youtube#playlistListResponse" then
      local plinfo = (t.items or E)[1]
      if plinfo then
        assert(plinfo.kind == "youtube#playlist", 'playlist kind')
        out = {
          title = (plinfo.snippet or E).title,
          videos = (plinfo.contentDetails or E).itemCount,
          channel = (plinfo.snippet or E).channelId,
        }
      end
    end
  end

  return out
end

--
-- parse html body to fetch json ytInitialData with links
--
---@param body string
---@param varname string - js variable name in <script> block
---@return false|table
---@return string?
function M.parse_html_to_get_value_in_script(body, varname)
  assert(type(body) == 'string' and body ~= '', 'body')
  if not body then
    print('not found file')
    return false
  end
  varname = varname or 'ytInitialData' -- js variable name in <script> block
  local ps, pe
  _, ps = string.find(body, varname .. ' = {', 1, true)
  if ps then
    pe = string.find(body, '</script>', ps, true)
  end

  if ps and pe and pe > ps then
    local ch = body:sub(pe - 1, pe - 1)
    if ch == ';' then pe = pe - 1 end
    local json = string.sub(body, ps, pe - 1)
    local _, t = pcall(cjson.decode, json)
    if type(t) == 'table' then
      return t, nil
    end
  end

  return false
end

--
-- from yt/@channel/videos
--
---@param url string
---@param limit number?
function M.fetch_vids_links(url, proxy, limit)
  local host, channel, sub = string.match(url or '', '^https://([^/]+)/@([^/]+)/?([^/]*)')
  if not host or not channel then
    return false, 'no host or channel'
  end
  if sub == nil or sub == '' then
    url = url .. '/videos'
  end

  local ok, resp = M.send_get(url, nil, proxy)
  if not ok or type(resp) ~= 'string' then
    return false, 'cannot fetch yt response got resp:' .. type(resp)
  end
  local t, err = M.parse_html_to_get_value_in_script(resp, 'ytInitialData')
  if type(t) ~= 'table' then
    return false, v2s(err)
  end
  -- base.write('/tmp/videos.txt', resp, 'wb')

  local path_to_content = {
    'contents', 'twoColumnBrowseResultsRenderer', 'tabs', 2, 'tabRenderer',
    'content', 'richGridRenderer', 'contents'
  }
  local contents, err2 = M.open_node(t, path_to_content)
  if not contents then
    return false, err2
  end

  local path_to_url = {
    'richItemRenderer', 'content', 'videoRenderer', 'navigationEndpoint',
    'commandMetadata', 'webCommandMetadata', 'url'
  }

  local links = {}
  for _, e in ipairs(contents) do
    local v = M.open_node(e, path_to_url)
    links[#links + 1] = v
  end

  -- todo fetch all via continuation
  limit = limit

  return links, nil
end

--------------------------------------------------------------------------------

--
-- helper
---@return false|table
---@return string?
function M.open_node(t, path)
  local root = t
  for n, key in ipairs(path) do
    root = root[key]
    if not root then
      return false, 'cannot open key:' .. v2s(key) .. ' at depth:' .. v2s(n)
    end
  end
  return root, nil
end

---@param line string?
function M.parse_line_with_vid(line)
  if line == nil or #line < 11 then
    return nil
  end
  local match = string.match
  local vid = match(line, '^%s*https://www.youtube.com/watch%?v=([%w%-_]+)')
  if not vid and line and #line >= 11 then
    vid = match(line, "^%s*([%w%-_]+)%s*$") -- only vid
  end
  if not vid and line then
    vid = match(line, "/watch%?v=([%w%-_]+)")
  end
  return vid
end

--
-- extract id from url or as is
--
-- abcdef12345 -> abcdef12345
-- https://www.youtube.com/watch?v=abcdef12345   -->   abcdef12345
--
---@param id string
---@return string
function M.extract_id_from_url_or_as_is(id)
  if #id > M.PLID_LEN and string.find(id, "://", 1, true) ~= nil then
    local vid = string.match(id, "watch%?v=([^%&]+)")
    if vid ~= nil then
      return vid
    end
  end
  -- todo with playlist??

  return id
end

return M
