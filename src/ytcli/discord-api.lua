-- 24-02-2024 @author Swarg
--
-- Goal: send notifications to discord channel
--
-- https://discord.com/developers/docs/resources/webhook
--
local log = require('alogger')

local http = require("socket.http")
local ltn12 = require("ltn12")
local inspect = require("inspect")
local fmt = string.format

local webhook_url = 'https://discord.com/api/webhooks/'
--

local M = {}

---@param webhook_id string
---@param webhook_token string
---@param name string
---@param message string
---@param dry_run boolean?
function M.webhook_send(webhook_id, webhook_token, name, message, dry_run)
  print(name, message)
  local payload = fmt('{"username":"%s","avatar_url":"","content":"%s"}',
    name, message
  )

  local response_body = {}
  local new_reqt = {
    url = webhook_url .. webhook_id .. '/' .. webhook_token,
    method = "POST",
    headers = {
      ["Content-Type"] = "application/json",
      ["Content-Length"] = payload:len()
    },
    source = ltn12.source.string(payload),
  }
  if dry_run then
    print('[DRY_RUN]', inspect(new_reqt))
    return
  end

  local r, code, headers, status = http.request(new_reqt)
  if not r then
    log.debug('http.req err:' .. tostring(code))
    return false
  end

  if code ~= 200 and code ~= 204 then
    log.debug('response code is', code, status)
    print('code:', code, status)
    print('[DEBUG]', payload)
    print(inspect(headers))
    local resp_body = table.concat(response_body)
    print(resp_body)
    return false
  end

  local resp_body = table.concat(response_body)
  log.debug("resp-body len:", #resp_body)

  return true, resp_body
end

--
--
---@param url string
function M.parse_webhook_url(url)
  local id, token = string.match(url, 'discord.com/api/webhooks/([^/]+)/(.*)$')
  return id, token
end

return M
