-- 22-02-2024 @author Swarg
--
-- App Configuration
--
local log = require 'alogger'

local base = require 'cli-app-base'
local builtin = require 'cli-app-base.builtin-cmds'

local M = {}

M.appname = 'ytcli'
M.config_file = nil
M.config = nil

M.CONF_DIR = os.getenv('HOME') .. '/.config/' .. M.appname .. '/'
M.SHARE_DIR = os.getenv('HOME') .. '/.local/share/' .. M.appname .. '/'
M.SUBS_DIR = 'subs'

-- used to download media files
M.URL_YT_DLP = "https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp"
M.BIN_YT_DLP = "/usr/local/bin/yt-dlp"

local E = {}

local default_config = {
  editor = 'vi', -- nvim
  logger = {
    save = true,
    level = log.levels.INFO,
    appname = M.appname
  },
  downloading = {
    formats_extractor = 'yt_dlp', -- fetch_formats_via_yt_dlp = true,
    download_retries = 32,        -- default in yt-dlp is 10
    dump_formats = false,         -- debugging
    yt_dlp = {
      -- defaults is android,web,ios
      extractor_args = 'youtube:player_client=android,web', -- "%s"
    },
  },
  proxy = nil, -- '10.0.0.1:8080'
}

---@param conf table
local function apply_env_vars(conf)
  conf = conf
  local debug_log_level = os.getenv('YTCLI_DEBUG')
  if debug_log_level then
    log.fast_setup()
  end
  if os.getenv('DUMP_FORMATS') then
    conf.downloading = conf.downloading or {}
    conf.downloading.dump_formats = true
  end
end

function M.setup()
  M.config, M.config_file = base.setup(
    default_config, M.CONF_DIR, M.SHARE_DIR, apply_env_vars
  )
  -- bind self to buildint commands such as logger, config
  builtin.settings = M
end

--
-- save config file
--
---@param verbose boolean?
---@return boolean
function M.save(verbose)
  local f = base.save_config(M.config_file, M.config)
  local msg = M.config_file .. ' saved: ' .. tostring(f)
  if verbose then
    print(msg)
  end
  return f
end

function M.get_discord_webhooks()
  M.config.discord = M.config.discord or {}
  M.config.discord.webhooks = M.config.discord.webhooks or {}
  return M.config.discord.webhooks
end

--

local function get_conf_downloading()
  return (M.config or E).downloading
end

function M.get_download_retries()
  return (get_conf_downloading() or E).download_retries
end

---
---@return boolean
function M.is_dump_formats()
  return (get_conf_downloading() or E).dump_formats == true
end

function M.get_extractor_args()
  local t = (get_conf_downloading() or E)
  local name = t.formats_extractor
  return (t[name or false] or E).extractor_args
end

return M
