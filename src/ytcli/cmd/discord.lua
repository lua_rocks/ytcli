-- 24-02-2024 @author Swarg
--
local settings = require('ytcli.settings')
local discordapi = require("ytcli.discord-api")

local M = {}
--

---@param w Cmd4Lua
function M.handler(w)
  w:about('Discord')
      :handlers(M)

      :desc('add webhook')
      :cmd('add', 'a')

      :desc('list of webhooks')
      :cmd('list', 'ls')

      :desc('send message to channel via webhook')
      :cmd('send-message', 'sm')

      :run()
end

--
-- add webhook
--
---@param w Cmd4Lua
function M.cmd_add(w)
  w:v_opt_verbose('-v')
  local name = w:desc('webhook name'):tag('name'):pop():arg()
  local url = w:desc('webhook url'):tag('url'):pop():arg()

  if not w:is_input_valid() then return end

  if #name > #url then -- swap
    local tmp = url
    url = name
    name = tmp
  end

  local webhook_id, token = discordapi.parse_webhook_url(url)
  if not webhook_id then
    return w:error('webhook: cannot parse url: ' .. tostring(url))
  end

  local t = settings.get_discord_webhooks()
  t[name] = { id = webhook_id, token = token }

  if settings.save(w:is_verbose()) then
    print('saved webhook:', name, webhook_id)
  end
end

--
-- list of webhooks
--
---@param w Cmd4Lua
function M.cmd_list(w)
  w:v_opt_verbose('-v')
  if not w:is_input_valid() then return end

  for name, t in pairs(settings.get_discord_webhooks()) do
    local line = name .. '  ' .. tostring(t.id)
    if w:is_verbose() then
      line = line .. '  ' .. tostring(t.token)
    end
    print(line)
  end
end

--
--
---@param w Cmd4Lua
function M.cmd_send_message(w)
  w:v_opt_dry_run('-d')
  local name = w:desc('webhookname'):pop():arg()
  local msg = w:desc('message to send'):pop():argl()

  if not w:is_input_valid() then return end

  local whs = settings.get_discord_webhooks()
  local wh = whs[name]
  if not wh then
    return w:error('not found webhook for name: ' .. tostring(name), 1)
  end
  assert(wh.id, 'has webhook id')
  assert(wh.token, 'has webhook token')

  local sender = settings.appname or 'Notifier'
  msg = table.concat(msg, ' ')
  discordapi.webhook_send(wh.id, wh.token, sender, msg, w:is_dry_run())
end

return M
