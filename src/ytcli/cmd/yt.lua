-- 22-02-2024 @author Swarg
--
local log = require('alogger')
local base = require('cli-app-base')
local settings = require('ytcli.settings')

local ytapi = require("ytcli.yt-api")
local yt_download = require 'ytcli.download'
local discordapi = require("ytcli.discord-api")
local usubtitles = require("ytcli.subtitles")
local usubscriptions = require("ytcli.subscriptions")

local M = {}
local ID_FILENAME = './id'
local DEF_AUDIO_F = 'a48k'
local DEF_VIDEO_F = '720p'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug

--------------------------------------------------------------------------------
--  See interface in ytcli/handler.lua
--------------------------------------------------------------------------------


local function get_cache_dir()
  local dir = base.join_path(settings.SHARE_DIR or '/tmp', '/cache/')
  if not base.dir_exists(dir) then
    local success = base.mkdir(dir)
    log.debug('mkdir %s: %s', dir, success)
  end
  return dir
end

--
--
---@param vid string video-id
---@param opts table?{nocache, reload, dry_run}
---@return string? content
---@return string filename
local function get_video_info_raw(vid, opts)
  opts = opts or {}
  local content = nil
  local fn = get_cache_dir() .. tostring(vid) .. '.json'

  if not opts.reload and not opts.no_cache and base.file_exists(fn) then
    log.debug('read from cache', fn)
    content = base.read_all(fn)
  else
    log.debug('pull... ', vid)
    local st, ret = ytapi.get_video_info(vid)

    if st and ret then
      content = ret
      if not opts.no_cache then
        local saved = base.write(fn, ret, 'w', opts.dry_run)
        log.info('saved: %s %s', saved, fn)
      end
    else
      log.info('cannot pull info for: %s', vid)
    end
  end

  return content, fn
end

--
--
--
---@param vid string
local function clear_cached(vid)
  local dir = settings.SHARE_DIR or '/tmp/'
  local fn = dir .. tostring(vid)
  if base.file_exists(fn) then
    return 'cleaned:' .. tostring(os.remove(fn))
  end
  return 'not found in ' .. tostring(fn)
end

--
--
--
---@param w Cmd4Lua
local function define_shared_opt_videoid(w)
  w:v_opt_verbose('-v')
  w:v_opt_dry_run('-d')

  local vid = w:desc('video-id'):pop():arg()

  w:desc('do not use disk cache to store json')
      :v_has_opt('--no-cache', '-C')

  w:desc('force to reload if has already cached json')
      :v_has_opt('--reload', '-R')

  return vid
end

--
-- get video info by id
--
---@param w Cmd4Lua
function M.cmd_video_info(w)
  log.debug("cmd_video_info")
  local vid = define_shared_opt_videoid(w)
  local raw = w:desc('show raw json'):def(1):has_opt('--raw-json', '-j')

  local onlysize = w:desc('show only size of fetched json')
      :has_opt('--only-size', '-S')

  local edit = w:desc('open cached json in the editior')
      :has_opt('--edit', '-r')

  if not w:is_input_valid() then return end

  local json, fn = get_video_info_raw(vid, w.vars)

  if onlysize then
    print(vid, #(json or ''))
  elseif edit then
    print('open', fn, base.open_file(settings.config, fn))
  elseif not raw and json then
    local readable = ytapi.get_readable_video_info(json)
    print(readable)
  else
    print(json)
  end
end

---@param w Cmd4Lua
function M.cmd_clear_cache(w)
  w:v_opt_dry_run('-d')
  local vid = w:desc('video-id'):pop():arg()

  if not w:is_input_valid() then return end
  print(clear_cached(vid))
end

--
-- debugging: uncompress given file
--
---@param w Cmd4Lua
function M.cmd_ungzip(w)
  local file = w:desc('gzip-file to uncompress'):pop():arg()

  if not w:is_input_valid() then return end

  print(ytapi.ungzip(file))
end

--------------------------------------------------------------------------------
--
local subs = {}

--
-- get subtitles for given video
--
---@param w Cmd4Lua
function M.cmd_subtitles(w)
  w:about('Subtitles')
      :handlers(subs)

      :desc('list subtitles for given video')
      :cmd('list', 'ls')

      :desc('get subtitles for given video')
      :cmd('get', 'g')

      :desc('convert subtitles')
      :cmd('convert', 'c')

      :run()
end

--
-- subtitles list
--
---@param w Cmd4Lua
function subs.cmd_list(w)
  local vid = define_shared_opt_videoid(w)

  if not w:is_input_valid() then return end

  local str = get_video_info_raw(vid, w.vars)
  if not str then
    return w:set_exitcode(1)
  end
  local t, err = ytapi.get_subtitles_list(str, w:is_verbose())
  if not t then
    return w:error(v2s(vid) .. ' ' .. v2s(err))
  end
  print(ytapi.fotmat_subtitles(t, w:is_verbose()))
end

--
-- show selected subs with other
--
---@param t table?
---@param i number
local function show_selected(t, i)
  local info

  if t and #t > 1 then
    local s = 'variants:'
    for j, tsub in ipairs(t) do
      if j == i then -- selected
        s = s .. ' >>' .. tostring(tsub.name) .. '<< '
      else
        s = s .. ' ' .. tostring(tsub.name) .. ' '
      end
    end
    info = s
  else
    info = (t or {})[i]
    if type(info) == 'table' then
      info = info.name or '?'
    end
  end

  return info
end


--
-- if instead video-id given index of file in current directory
--
---@param vid string
local function pick_video_id_by_index(vid)
  local index = tonumber(vid)
  if index then
    local t = base.execrl('ls ' .. vid .. "*")
    if t and #t > 0 then
      local name = t[1]
      if #t > 1 then
        local msg = ''
        for i, fn in pairs(t) do
          msg = msg .. string.format("%2d - %s", i, fn) .. "\n"
        end
        msg = msg .. 'Your choise? '
        local select = base.ask_value(msg, "1")
        local i0 = tonumber(select)
        if i0 then
          name = t[i0]
        else
          print('Canceled')
          os.exit(1)
        end
      end
      vid = ytapi.get_vid_from_filename(base.filename_without_extension(name))
      log.debug('vid:%s from name:%s', vid, name)
    end
  end
  return vid
end

--
--
---@param w Cmd4Lua
---@param vid string
---@param subsdir string?
local function get_video_subs(w, vid, subsdir)
  local opts = w.vars or {}

  local ext = 'xml'
  if not opts.raw then
    ext = 'vtt'
  end
  local fn = vid .. '.' .. ext
  if subsdir or base.dir_exists(settings.SUBS_DIR) then
    subsdir = subsdir or settings.SUBS_DIR
    fn = base.join_path(subsdir, fn)
  end

  if base.file_exists(fn) then
    print('[SKIP] file already exists:', fn)
    return false
  end

  local json = get_video_info_raw(vid, opts) -- reload no_cache dry_run
  if not json then
    return w:error('Cannot get video-info: ' .. v2s(vid), 1)
  end

  local t, err = ytapi.get_subtitles_list(json)
  if not t then
    return w:error(v2s(vid) .. ' ' .. v2s(err))
  end

  local sub = (t or {})[opts.index]
  if not sub then
    return w:error('Cannot find subtitles for index ' .. tostring(opts.index), 1)
  end

  if not sub.name or not sub.url then
    return w:error('[ERROR] wrong sub: opts: '
      .. require "inspect" (opts) ..
      'sub_list: ' .. require "inspect" (t))
  end

  log.debug("%s%s", sub.name, sub.url)
  print(show_selected(t, opts.index))

  local st, body = ytapi.send_get(sub.url, 'text/xml')
  if not st then
    return w:error('Cannot download subtitles', 1)
  end

  if not opts.raw then
    -- convert to WEBVTT
    local ok, convert = false, usubtitles.convert_xml_subs_to_vtt
    ok, body = pcall(convert, body)
    if not ok then
      error('error on ' .. tostring(vid) .. tostring(body))
      os.exit(1)
    end
    -- body = usubtitles.convert_xml_subs_to_vtt(body) ---@cast body string
  end

  local saved = body ~= nil and base.write(fn, body, 'wb', w:is_dry_run())
  print(fn, 'saved:', saved)

  if not opts.no_info then
    local ibody = ytapi.get_readable_video_info(json)
    fn = base.filename_without_extension(fn) .. ".md"
    saved = ibody ~= nil and base.write(fn, ibody, 'wb', w:is_dry_run())
    print(fn, 'saved:', saved)
  end
end

--
-- subtitles get
--
---@param w Cmd4Lua
function subs.cmd_get(w)
  w:usage('ytcli subtitles get videoId')
  w:usage('ytcli subtitles get all')

  local vid = define_shared_opt_videoid(w)
  w:desc('subs index in the list'):def(1):v_optn('--index', '-i')
  w:desc('Do not convert to vtt'):v_has_opt('--raw', '-r')
  w:desc('do not extract and save video-info to md file(subtitles only)')
      :v_has_opt('--no-info', '-I')

  if not w:is_input_valid() then return end

  if vid == "all" then
    local t = ytapi.get_vids_from_path('.')
    local subsdir = settings.SUBS_DIR
    if not base.mkdir(subsdir) then
      return w:error('Cannot create dir: ' .. subsdir, 1)
    end

    for i, vid0 in ipairs(t) do
      print(v2s(i) .. '/' .. v2s(#t) .. ' ' .. vid0 .. ' pull subtitles...')
      w.vars.no_cache = true
      get_video_subs(w, vid0, subsdir)
    end
    return
  end

  vid = pick_video_id_by_index(vid)
  get_video_subs(w, vid)
end

--
-- convert subtitles
--
---@param w Cmd4Lua
function subs.cmd_convert(w)
  local fn = w:desc('filename with subscribtions to convert')
      :opt('--file', '-i')
  local format = w:desc('target subscribtion format')
      :def('vtt'):opt('--format', '-f')

  if not w:is_input_valid() then return end

  if format ~= 'vtt' then
    return w:error('Not Supported subtitles format ' .. tostring(format))
  end

  local body = base.read_all(fn)
  if not body then
    return w:error('Cannot read ' .. tostring(fn))
  end

  -- raw yt xml to vtt
  body = ytapi.convert_xml_subs_to_vtt(body)
  local ext = 'vtt'

  fn = fn .. '.' .. ext

  local saved = body ~= nil and base.write(fn, body, 'wb', w:is_dry_run())
  print('saved:', saved, fn)
end

--
--
-- F12: ytInitialData.metadata.channelMetadataRenderer.externalId
--
---@param w Cmd4Lua
function M.cmd_channel_info(w)
  local cid = w:desc('chennel-id'):pop():arg()

  local size = w:desc('show only size of response')
      :has_opt('--item-count', '-c')
  local save = w:desc('save raw responce body')
      :has_opt('--save-responce', '-s')

  if not w:is_input_valid() then return end

  local st, body = ytapi.get_channel_info(cid, M.get_apikey())

  if st and body and size then
    print('size:', #body)
  else
    if save and body then
      local fn = cid .. '.json'
      local saved = base.write(fn, body, 'wb', w:is_dry_run())
      print('saved:', saved, fn, #body)
      return
    end

    print(body)
  end
end

--
-- get api key from envvar APIKEY or config-file
-- use ytcli apikey add before or add manyaly to a config file
--
---@return string
function M.get_apikey()
  local apikey = os.getenv('APIKEY')
  if apikey ~= nil and apikey ~= '' then
    log.debug('API key from EnvVar', apikey)
  else
    apikey = ((settings or E).config or E).api_key
    log.debug('API key from Config:', apikey)
  end
  assert(type(apikey) == 'string' and apikey ~= '', 'has apikey v3')

  return apikey
end

--
--
--
---@param w Cmd4Lua
function M.cmd_playlist_info(w)
  local plid = w:desc('playlist-id'):pop():arg()
  local size = w:desc('playlist items(videos) count')
      :has_opt('--item-count', '-c')

  local save = w:desc('save raw responce body')
      :has_opt('--save-responce', '-s')

  local apiver = w:desc('api version')
      :def('v1'):opt('--api', '-a')

  if not w:is_input_valid() then return end

  local st, body
  if apiver == 'v3' then
    st, body = ytapi.get_playlist_info_v3(plid, M.get_apikey())
  else
    st, body = ytapi.get_playlist_info(plid)
  end

  if st and body and size then
    print('items-count:', ytapi.get_playlist_item_count(body))
  else
    if save and body then
      local fn = plid .. '.json'
      local saved = base.write(fn, body, 'wb', w:is_dry_run())
      print('saved:', saved, fn, #body)
      return
    end
    print(body)
  end
end

--------------------------------------------------------------------------------


function M.cmd_api_key(w)
  w:about('API-KEY')

      :desc('add api-key to config')
      :cmd('add', 'a', M.cmd_add_api_key)

      :cmd('show', 's', M.cmd_show_api_key)

      :run()
end

--
-- for api v3
-- api v1 can work without any key
--
---@param w Cmd4Lua
function M.cmd_add_api_key(w)
  w:v_opt_dry_run('-d')
  local key = w:desc('api-key for yt v3'):tag('api-key'):pop():arg()

  if not w:is_input_valid() then return end

  local f
  settings.config.api_key = key
  f = base.save_config(settings.config_file, settings.config, w:is_dry_run())
  print(settings.config_file, 'saved:', f)
end

--
---@param w Cmd4Lua
function M.cmd_show_api_key(w)
  if not w:is_input_valid() then return end
  print(settings.config.api_key)
end

--------------------------------------------------------------------------------

---@param w Cmd4Lua
function M.cmd_subscriptions(w)
  w:about('subscription to a new content')

      :cmd('add', 'a', M.cmd_subscriptions_add)

      :cmd('list', 'ls', M.cmd_subscriptions_list)

      :cmd('check', 'c', M.cmd_subscriptions_check)

      :desc('new content tracking mode')
      :cmd('track', 't', M.cmd_subscriptions_track)

      :run()
end

--
--
--
---@param w Cmd4Lua
function M.cmd_subscriptions_add(w)
  w:v_opt_verbose('-v')
  local id = w:desc('new subscribtion id(playlist or channel)'):pop():arg()

  if not w:is_input_valid() then return end

  local t = settings.config.subscription
  t = t or {}
  if base.tbl_index_of(t, id) >= 0 then
    print('already exists', id)
    return
  end
  t[#t + 1] = id
  settings.config.subscription = t
  print(settings.save(w:is_verbose()))
end

--
--
--
---@param w Cmd4Lua
function M.cmd_subscriptions_list(w)
  if not w:is_input_valid() then return end
  for _, v in pairs(settings.config.subscription) do
    print(v)
  end
end

--
--
--
---@param w Cmd4Lua
function M.cmd_subscriptions_check(w)
  w:v_opt_verbose('-v')
  if not w:is_input_valid() then return end

  local apikey = M.get_apikey()
  for _, id in pairs(settings.config.subscription) do
    if id:sub(1, 2) == 'PL' then
      local t = ytapi.subscription_check_playlist(apikey, id)
      local line = usubscriptions.build_notify_msg_playlist(t, w:is_verbose())
      print(line)
    end
  end
end

--
-- new content tracking mode
--
---@param w Cmd4Lua
function M.cmd_subscriptions_track(w)
  w:v_opt_dry_run('-d')
  w:v_opt_verbose('-v')

  local delay_sec = w:desc('delay time after which to check the state (sec)')
      :def((settings.config or E).delay_sec or 60):optn('--delay-time', '-t')
  local one_shot = w:desc('only one shot and exit from loop(debugging)')
      :has_opt('--one-shot', '-O')
  local discord_channel = w:desc('discord channel name to send notifications')
      :opt('--discord-channel', '-c')

  if not w:is_input_valid() then return end

  -- pick the discord webhook to which notifications will be sent
  local whs = settings.get_discord_webhooks()
  local wh = whs[discord_channel]
  if not wh and next(whs) then
    wh = whs[next(whs) or false] -- next give keyname not a value
    w:verbose('take first webhook from config', wh.id)
  end
  if not wh then
    return w:error('cannot found discord webhook to send notifications')
  end

  assert(wh.id, 'webhook-id')
  assert(wh.token, 'webhook-token')

  local sender = settings.appname or 'Notifier'

  local ctl = {
    dir = settings.SHARE_DIR,
    config = settings.config,
    apikey = M.get_apikey(),
    verbose = w:is_verbose(),
    delay_sec = delay_sec,
    dry_run = w:is_dry_run(),
    stop = one_shot,
    notifier = function(msg)
      -- todo split big msg > 2000 chars
      discordapi.webhook_send(wh.id, wh.token, sender, msg)
    end,
  }
  local cnt = #(settings.config.subscription or E)
  local prefix = ctl.dry_run == true and '[DRY_RUN] ' or ''
  print(fmt('%swebhook[%s] tracking(%s)...', prefix, tostring(wh.id), cnt))
  usubscriptions.loop_check_news(ctl)
end

-------------------------------------------------------------------------------

---@return false|string
local function pick_from_id_file()
  if base.file_exists(ID_FILENAME) then
    return (base.read_first_lines(ID_FILENAME, 1) or E)[1] -- from first line
  end
  return false
end

---@param fn string
---@return false|table
local function pick_from_ids_file(w, fn)
  if not base.file_exists(fn) then
    return false, w:error('file not found: ' .. v2s(fn))
  end
  local t = {}
  for line in io.lines(fn) do
    t[#t + 1] = ytapi.parse_line_with_vid(line)
  end

  if w:is_verbose() then
    w:verbose("ids from file: ", table.concat(t, ' '))
  end
  if #t == 0 then
    return false, w:error('not found any vid in file:' .. v2s(fn))
  end

  return t
end

--
-- download one video by its videoId or multiple videos from given playlistId
--
---@param w Cmd4Lua
function M.cmd_download(w)
  w:usage('ytcli download <VideoID> --formats [139 257]')
  w:usage('ytcli download <PlaylistID> -f [139 257] --items 1-10')
  w:usage('ytcli download <PlaylistID> -f [a 720p] --items [1-5 8 10-15]')
  w:usage('ytcli download <PlaylistID> -f [a 720x1280] --items [1 2 3 4 5 8 9]')
  w:usage('ytcli d <VID> -f def --only-web-extractor')
  w:usage('ytcli d <VID> -f def --extractor-args "youtube:player_client=android,web,ios"')
  w:usage('ytcli d file:vids_list_one_per_line -f def')

  w:v_opt_dry_run('-d')
  w:v_opt_verbose('-v')

  local formats = w:desc('audio and video formats for download (itag:number)')
      :def({}):optl('--formats', '-f')

  -- debugging
  w:desc('show urls only'):v_has_opt('--show-urls', '-u')
  w:desc('show all mime-types only'):v_has_opt('--mime-types-only', '-M')
  w:desc('only calculate size w/o downloading'):v_has_opt('--only-size', '-Z')

  local id = w:desc('videoId or playlistId or "." to pick playlistId from file')
      :pop():arg()
  local items = w:desc('playlist items'):optl('--items', '-i')
  -- case then in one playlis has dubs from another already downloaded playlist
  w:desc('list of directories to skip already downloaded')
      :v_optl('--skip-already-downloaded', '-S')

  -- experimental
  w:desc('android,web,ios'):v_opt('--extractor-args', '-E')

  if not w:is_input_valid() then return end

  local ok, ret
  w.vars.ask = true -- ask the user for formates if not given
  if formats[1] == 'def' then formats = { DEF_AUDIO_F, DEF_VIDEO_F } end

  -- pick playlistid from id file in same directory
  local vid_list
  if id == '.' then
    id = pick_from_id_file()
    if not id then
      return w:error('not found id-file: ' .. v2s(ID_FILENAME))
    end
    w:verbose("id from id-file:%s", v2s(id))
  elseif id and id:sub(1, 5) == 'file:' then
    vid_list = pick_from_ids_file(w, id:sub(6, #id))
    if not vid_list then return end -- show err
  end
  M.normalize_formats(formats)
  if vid_list then
    for _, vid in ipairs(vid_list) do
      ok, ret = yt_download.download_vid({ vid = vid }, formats, w.vars)
      if not ok then
        w:error(ret or ('on vid:' .. v2s(vid)))
      end
    end
  else
    id = ytapi.extract_id_from_url_or_as_is(id)

    if #id == ytapi.VID_LEN then
      ok, ret = yt_download.download_vid({ vid = id }, formats, w.vars)
      --
    elseif #id == ytapi.PLID_LEN then
      ok, ret = yt_download.download_playlist(id, formats, items, w.vars)
      --
    else
      ok, ret = false, fmt('unknown id-size: %s id:"%s"', v2s(#id), v2s(id))
    end
  end

  if not ok then return w:error(v2s(ret or 'error:?')) end

  if w.vars.only_size then ret = M.show_size_report(w, ret) end

  w:say(ret)
end

--
-- case  136,139 instead of -f [136 139] or a49k,v
-- std way to define list is -f [elm1 elm2 elmN]
-- this thing checks user input: -f elm1,elm2 and split by ,
function M.normalize_formats(t)
  if type(t) == 'table' and #(t) == 1 and type(t[1]) == 'string' then
    local i1, i2 = string.match(t[1], '^(%w+),(%w+)$')
    if i1 and i2 then
      t[1], t[2] = i1, i2
    end
  end
end

--
---@param w Cmd4Lua
function M.show_size_report(w, prefix)
  local t = w.vars.report
  log_debug("show_size_report report sz:", #(t or E))
  if not t then
    return w:error('empty report')
  end
  local s, c = '', 0
  if prefix then s = v2s(prefix) .. ' ' end

  for _, e in ipairs(t) do
    s = s .. fmt("%-10s %10.2f Mb\n", v2s(e.itag), (e.sz or 0) / (1024 * 1024))
    c = c + (e.sz or 0)
  end

  return s .. fmt("%-10s %10.2f Mb\n", 'total', (c) / (1024 * 1024))
end

--
-- install dependency to download media files
--
---@param w Cmd4Lua
function M.cmd_install_yt_dlp(w)
  w:usage('ytcli install-yt-dlp --check  # only check the latest release version')

  w:v_opt_dry_run('-d')
  local check_new = w:desc('only check new release'):has_opt('--check', '-c')

  if not w:is_input_valid() then return end

  local url, bin, cmd = settings.URL_YT_DLP, settings.BIN_YT_DLP, nil

  local show_version = ' && echo Installed yt-dlp version: `yt-dlp --version`'

  if check_new then
    cmd = fmt('curl -s -w "%%{redirect_url}\n" "%s"', url) .. show_version
  else
    cmd = fmt('sudo wget -O "%s" "%s" && chmod +x "%s"', bin, url, bin) ..
        show_version
  end
  base.exec(cmd, w:is_dry_run())
end

--
-- get videos list for given channel
-- build list of vid for given channel
--
---@param w Cmd4Lua
function M.cmd_videos_list(w)
  local url = w:desc('url to yt channel'):pop():arg()
  local proxy = w:desc('via proxy'):opt('--proxy', '-p')

  if not w:is_input_valid() then return end
  local conf = settings.config or E

  if not proxy and type(conf.proxy) == 'string' and conf.proxy ~= '' then
    proxy = proxy or conf.proxy
  end
  if proxy == 'no' or proxy == '' then
    proxy = nil
  end

  local ret, err = ytapi.fetch_vids_links(url, proxy)
  if type(ret) ~= 'table' then
    return w:error('error:' .. require "inspect" (err))
  end

  for _, link in ipairs(ret) do
    w:say(link)
  end
end

--
return M
