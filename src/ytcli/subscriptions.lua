-- 25-02-2024 @author Swarg

local log = require('alogger')
local base = require('cli-app-base')
local socket = require("socket")

local ytapi = require("ytcli.yt-api")

local fmt = string.format

local M = {}
--
---@param t table
---@param verbose boolean?
function M.build_notify_msg_playlist(t, verbose)
  local line = fmt('%s: %s', tostring(t.title), tostring(t.videos))
  if verbose then
    line = line .. '  ' .. tostring(t.channel)
  end
  return line
end

--
-- build actual current state for all subscriptions
--
---@param subscriptions table
---@param apikey string
function M.get_current_state(subscriptions, apikey)
  local state = {}
  for _, id in pairs(subscriptions) do
    if id:sub(1, 2) == 'PL' then
      state.playlists = state.playlists or {}
      state.playlists[id] = ytapi.subscription_check_playlist(apikey, id)
    end
  end
  return state
end

--
-- check is in curr_state has changes(news|undates) if so build notify message
-- make diff prev_state & state with prev_state update to actual state
--
---@param curr_state table
---@param verbose boolean?
function M.diff_and_build_notify_message(prev_state, curr_state, verbose)
  -- find updates in playlists
  local msg, changed = '', false
  prev_state.playlists = prev_state.playlists or {}

  for id, t in pairs(curr_state.playlists) do
    local pt = prev_state.playlists[id]
    if not pt or pt.videos ~= t.videos then
      prev_state.playlists[id] = t -- keep new state
      if #msg > 0 then msg = msg .. "\n" end
      msg = msg .. M.build_notify_msg_playlist(t, verbose)
      changed = true
    end
  end
  return msg, changed
end

--
-- save message if has and not empty and save updated state
--
---@param ctl table{verbose, notifier}
---@param msg string
function M.send_message_if_has(ctl, msg)
  if msg and #msg > 0 then -- has-news
    base.vprint(ctl.verbose, '[SendNews]')
    ctl.notifier(msg)
    return true
  end
  return false
end

--
--
---@param filename string
---@param state table
function M.save_state(filename, state)
  log.debug('save state')
  base.save_lua_table(state, filename) -- save updated state
end

--
-- loop track changes in subscriptions and send notifications
--
---@param ctl table{apikey, delay_sec, dir}
function M.loop_check_news(ctl)
  log.debug("loop_check_news >")
  assert(type(ctl) == 'table', 'has control')
  assert(type(ctl.notifier) == 'function', 'has notifier')

  local delay_sec, verbose = ctl.delay_sec or 60, ctl.verbose or ctl.dry_run

  if delay_sec < 5 then delay_sec = 5 end
  local dir = ctl.dir or '/tmp/'
  local subscriptions = ctl.config.subscription
  local state_storage = base.join_path(dir, 'prev_state.lua')

  base.vprint(verbose, state_storage, 'one-shot:', ctl.stop, "\n")
  if ctl.dry_run then
    local inspect = require "inspect"
    print("[DRY_RUN] delay between checking for updates:", delay_sec, 'sec')
    print("[DRY_RUN] subscriptions:", inspect(subscriptions))
    print("[DRY_RUN] prev_state: ", inspect(base.load_lua_table(state_storage)))
    return
  end

  if not subscriptions or not next(subscriptions) then
    return base.vprint(verbose, 'nothing to track (empty subscriptions)')
  end

  repeat
    -- TODO: read only if lastmodified time has changed
    local prev_state = base.load_lua_table(state_storage) or {}

    if prev_state.stop then -- stop loop via prev_state.lua file
      prev_state.stop = nil
      base.save_lua_table(prev_state, state_storage)
      return
    end

    local state = M.get_current_state(subscriptions, ctl.apikey)
    local msg = M.diff_and_build_notify_message(prev_state, state, verbose)
    if M.send_message_if_has(ctl, msg) then
      M.save_state(state_storage, prev_state)
    end

    base.vprint(verbose, '.')
    pcall(socket.sleep, delay_sec)
  until ctl.stop

  base.vprint(verbose, "\n")

  log.debug("< loop_check_news : exit")
end

return M
