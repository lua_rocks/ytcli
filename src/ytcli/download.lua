-- 24-06-2024 @author Swarg
-- get link to media file for downloading

local log = require 'alogger'
local base = require 'cli-app-base'
local uhttp = require 'cli-app-base.http'
local ytapi = require 'ytcli.yt-api'
local yt_dlp = require 'ytcli.util.yt_dlp'
local settings = require 'ytcli.settings'

local M = {}
local LOADER = 'yt-dlp' -- executable name
local YT_DLP = 'yt_dlp' -- key name in config

M.DEF_AUDIO_BITRATE = 42000
M.DEF_HEIGTH = 720
M.DEF_WIDTH = 1280

M.RESOLUTIONS = {
  ['144p'] = '144x256',
  ['360p'] = '360x640',
  ['720p'] = '720x1280',
  -- 420p
  ['1080p'] = '1080x1920',
}

local TRY_COUNT = 4
local SUCCESS = 'SUCCESS'
local ERROR = 'ERROR'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug

local conf = settings.config or E
local formats_extractor = (conf.downloading or E).formats_extractor or YT_DLP
local mk_proxy_opts = yt_dlp.mk_proxy_opts
-- local starts_with = base.str_starts_with

-- "audio/webm; codecs=\"opus\""  -->  "audio", "webm", 'codecs="opus"'
--                                       t       subt    codecs
---@param s string
function M.parse_mine_type(s)
  if type(s) == 'string' then
    local t, subt, codecs = string.match(s, '^([^%/]+)/([^;]+);%s*(.*)$')
    return t, subt, codecs
  end
end

function M.mk_format_entry(e, mt, subt, codecs)
  return {
    mime = { mt = mt, subt = subt, codecs = codecs },
    itag = e.itag,
    url = e.url,
    lastModified = e.lastModified,
    contentLength = e.contentLength,
    averageBitrate = e.averageBitrate,
    approxDurationMs = e.approxDurationMs,
    qualityLabel = e.qualityLabel,
    quality = e.quality,
    height = e.height,
    width = e.width,
    fps = e.fps,
  }
end

function M.comp_format_entries_asc(a, b)
  if type(a) == 'table' and type(b) == 'table' then
    local b1 = tonumber(a.averageBitrate)
    local b2 = tonumber(b.averageBitrate)
    return (b1 ~= nil and b2 ~= nil) and b1 < b2 or false
  end
  return false
end

function M.fmt_mime_type(m)
  return type(m) == 'table' and
      fmt("%s/%-5s %s", v2s(m.mt), v2s(m.subt), v2s(m.codecs)) or '?'
end

function M.get_mt_resolution(t)
  if t.height and t.width then
    return string.format('%sx%s', t.height, t.width)
  else
    return 'audio only'
  end
end

---@param t table
function M.fmt_media_format(t)
  if t then
    local sz = t.filesize or t.contentLength or 0
    local szmb
    if type(sz) == 'number' and sz > 1024 * 1024 then
      szmb = fmt("%7.2f Mb", sz / (1024 * 1024))
    else
      szmb = v2s(sz)
    end
    local resolution = M.get_mt_resolution(t)
    local ext = t.ext or '???'
    local br = (math.floor((t.averageBitrate or 0) / 1000)) .. 'k'
    local qu = t.qualityLabel or t.quality or ''
    local fps = t.fps == nil and '' or (t.fps .. 'fps')

    return fmt("%9s  %4s %-11s %5s %5s %11s %s %s",
      v2s(t.itag), ext, resolution, qu, br, szmb, M.fmt_mime_type(t.mime), fps
    )
  end
end

function M.fmt_formats(af, vf)
  local s = ''
  if af then
    for _, f in ipairs(af) do s = s .. M.fmt_media_format(f) .. "\n" end
  end
  if vf then
    for _, f in ipairs(vf) do s = s .. M.fmt_media_format(f) .. "\n" end
  end
  return s
end

---@param obj table
---@return table?, table?
function M.get_formats_vid(obj)
  local ok, t = base.tbl_get_node(obj, { 'streamingData', 'adaptiveFormats' })
  if not ok then return nil, nil end
  local vf, af = {}, {}
  for _, e in ipairs(t) do
    -- audio mp4, codecs=...
    local mt, subt, suff = M.parse_mine_type(e.mimeType)
    if mt == 'video' then
      vf[#vf + 1] = M.mk_format_entry(e, mt, subt, suff)
    elseif mt == 'audio' then
      af[#af + 1] = M.mk_format_entry(e, mt, subt, suff)
    end
  end
  return af, vf
end

--
-- test tooling
---@param obj table
function M.get_all_mime_types(obj)
  local ok, tt = base.tbl_get_node(obj, { 'streamingData', 'adaptiveFormats' })
  assert(ok, 'ok')

  local t = {}
  if ok then
    for _, e in ipairs(tt) do
      t[e.mimeType or false] = 1
      -- print("[DEBUG] mimeType:", e.mimeType)
    end
  end

  local s = "local types = {\n"
  for mt, _ in pairs(t) do
    s = s .. "'" .. mt .. "',\n"
  end
  return s .. '}'
end

---@param itag number
---@param list table
function M.find_format_by_itag(itag, list)
  -- assert(type(itag) == 'string', 'expected string format ID got: ' .. v2s(itag))
  assert(itag ~= nil and type(itag) ~= 'boolean', 'expected format itag got:' .. v2s(itag))
  for _, e in ipairs(list) do
    if e and e.itag == itag then
      return e
    end
  end
  return nil
end

---@return table? format entry
---@return string? ext
function M.find_format_by_itag_ex(itag, af, vf)
  local ext
  local e = M.find_format_by_itag(itag, af)
  if e then
    ext = e.ext == 'webm' and 'awebm' or e.ext -- to fix name conflict for a+vh
  else
    e = M.find_format_by_itag(itag, vf)
    ext = (e or E).ext
  end
  return e, ext
end

function M.is_format_audio_only(e)
  return type(e) == 'table' and (e.mime or E).mt == 'audio_only'
end

-- to ignore n-drc when search similar audio format
---@param a string itag 1
---@param b string itag 2
function M.some_drc_fmt(a, b)
  return a ~= nil and b ~= nil and match(a, '^(%d+)') == match(b, '^(%d+)')
end

---@param format table format
---@param af table
---@param vf table
---@return table?
function M.find_similar_format(format, af, vf)
  assert(type(format) == 'table', 'format')
  local itag = assert(format.itag, 'foramt.itag') -- string
  local mt = assert((format.mime or E).mt, 'format.mime.mt')

  local similar_format = nil

  if M.is_format_audio_only(format) then
    assert(mt == 'audio_only', 'sure audio_only')
    local abr = format.averageBitrate
    for _, e in ipairs(af) do -- for audio only
      if e and e.itag and e.mime and e.itag ~= itag and e.mime.mt == mt then
        -- mime = { codecs = "vp9", mt = "video_only", subt = "webm" },
        if e.averageBitrate and abr and not M.some_drc_fmt(e.itag, format.itag)
        then
          local diff = e.averageBitrate - format.averageBitrate
          if diff > 0 and ((diff * 100 / format.averageBitrate) <= 10) then
            similar_format = e
            break
          end
        end
      end
    end
  else -- for video only
    for _, e in ipairs(vf) do
      if e and e.mime and e.itag ~= itag and e.mime.mt == mt then
        if (e.height ~= nil and format.height == e.height) then -- video
          similar_format = e
          break
        end
      end
    end
  end
  log_debug("find_similar_format %s for %s", (similar_format or E).itag, itag)
  return similar_format -- not found
end

--
---@return table
function M.get_format_urls(af, vf, formats)
  local urls = {}
  for _, fi in ipairs(formats) do
    -- local n = tonumber(fi)
    -- if n ~= nil then
    local e = M.find_format_by_itag(fi, af) or M.find_format_by_itag(fi, vf)
    urls[#urls + 1] = (e or E).url
    -- end
  end
  return urls
end

---@param s string
function M.normalize_to_filename(s)
  if type(s) == 'string' then
    return s:gsub('%s', '_'):gsub('[:;,|/\\&%?><*()%[%]]', '')
        :gsub('«', ''):gsub('»', '')
  end
end

---@param params table{vid, title}
---@param ext string
---@return string
function M.build_output_fname(params, ext)
  if params and params.vid and params.title then -- from playlist info
    local pref = ''
    if params.index ~= nil then
      pref = fmt("%02d-", tonumber(params.index))
    end
    local title = M.normalize_to_filename(params.title)
    return fmt('%s%s-%s.%s', pref, title, v2s(params.vid), ext)
    -- todo add to queque for convert by ffmpeg
  end
  return "./%\\(title\\)s-%\\(id\\)s." .. ext
end

--
-- yt-dlp -f 249 -o ./%\(title\)s-%\(id\)s.awebm -- VideoId
---@param vid string
---@param itag string
---@param outname string template
---@param retries number
local function mk_cmd_download_format(vid, itag, outname, retries)
  -- redirect stderr to stdout + a way to get exitcode via string output
  local extra = "2>&1 && echo " .. SUCCESS .. " || echo " .. ERROR
  local progress_opts = "--progress-delta 1 --newline"
  local cmd
  -- if e.url and e.url ~= '' then
  --   -- via given direct url
  --   cmd = fmt("%s --retries %s -o %s '%s'",
  --     LOADER, retries, outname, e.url)
  -- else
  -- via format itag
  cmd = fmt("%s %s %s --retries %s -f %s -o %s -- %s %s",
    LOADER, progress_opts, mk_proxy_opts(), retries, itag, outname, vid, extra)
  -- `--` is a way to define argument begins with dash(-)
  -- end
  --
  return cmd
end

--
-- download one part of the media file (audio or video part)
---@param fmt_id any string 139, 139-drc, 139-dash
---@param af table
---@param vf table
---@param params table
---@param dry_run boolean?
function M.fetch_format_file(fmt_id, af, vf, params, dry_run)
  if fmt_id == 'na' or fmt_id == 'nv' then
    return true, 'skip'
  end

  local itag = fmt_id

  local e, ext = M.find_format_by_itag_ex(itag, af, vf)
  if not e then
    return false, 'not found available format for itag: ' .. v2s(fmt_id)
  end
  if not ext then
    return false, 'no extension for given itag format: ' .. v2s(fmt_id)
  end
  local outname = M.build_output_fname(params, ext)
  local retries = settings.get_download_retries() or 30
  local cmd = mk_cmd_download_format(params.vid, itag, outname, retries)
  if dry_run then
    print('[DRY_RUN]', cmd)
    return true
  end

  local try_n, status = 0, nil

  while try_n < TRY_COUNT and status == nil do
    try_n = try_n + 1
    print('try #', try_n, '/', TRY_COUNT, cmd)
    local fh = io.popen(cmd)
    if fh then
      while true do
        local ok_read, line = pcall(fh.read, fh, '*l')
        if not line then
          break         -- eof
        end
        if ok_read then -- processing
          print(line)
          if line == SUCCESS then
            status = SUCCESS
          end
          if match(line, 'ERROR') then
            if match(line, 'Requested format is not available') then
              local sf = M.find_similar_format(e, af, vf)
              if sf and sf.itag then
                e = sf
                cmd = mk_cmd_download_format(params.vid, sf.itag, outname, retries)
                -- try with new command
                break
              else
                status = 'BAD_FORMAT'
              end
            elseif line == ERROR then
              status = ERROR
            end
          end
        elseif not ok_read and line == 'interrupted!' then -- ?? todo check
          status = 'INTERRUPTED'
          break
        end
      end
      fh:close()
    end
  end -- while

  if status ~= SUCCESS then
    return false, fmt('cannot get fromat vid:%s itag:%s (%s)',
      params.vid, itag, v2s(status))
  end
  return true
end

--
-- download given format files for given vid(videoId)
--
---@param params table {vid:string}
---@param formats table
---@param af table
---@param vf table
---@param dry_run boolean?
function M.fetch_format_files_for_vid(params, formats, af, vf, dry_run)
  assert(type(params) == 'table', 'params')
  assert(type(params.vid) == 'string', 'params.vid')
  local errors = {}
  for _, fmt_num in ipairs(formats) do
    local ok, ret = M.fetch_format_file(fmt_num, af, vf, params, dry_run)
    if not ok then
      errors[#errors + 1] = ret
    end
  end
  if #(errors) > 0 then
    return false, table.concat(errors, "\n")
  end
  return true, 'done'
end

--
-- output placed into opts.report
--
---@return boolean
---@param formats table - list of selected formats(itags)
---@param af? table already available audio formats
---@param vf? table already available audio formats
---@param opts table{report}
function M.fetch_format_size_for_vid(formats, af, vf, opts)
  log_debug("fetch_format_size_for_vid", formats)
  assert(type(opts) == 'table', 'opts')
  opts.report = opts.report or {}
  local t = opts.report
  for _, fmt_num in ipairs(formats) do
    local e, _ = M.find_format_by_itag_ex(fmt_num, af, vf)
    if e then
      t[#t + 1] = { itag = fmt_num, sz = e.filesize, dur = 0 }
      log_debug("format-itag:%s size:%s", fmt_num, e.filesize)
    end
  end
  return true
end

-- [1-5 8 10-12] --> {1,2,3,4,5,8,10,11,12}
---@param items table
function M.parse_items(items)
  assert(type(items) == 'table', 't')
  local m = {}
  for _, item in ipairs(items) do
    local n = tonumber(item)
    if n then
      m[n] = true
    else
      -- range
      local s, e = string.match(item, '^(%d+)%-(%d+)$')
      if s and e then
        for i = s, e, 1 do m[i] = true end
      else
        -- case  4,8
        local i1, i2 = string.match(item, '^(%d+),(%d+)$')
        if tonumber(i1) and tonumber(i2) then
          m[tonumber(i1)], m[tonumber(i2)] = true, true
        end
      end
    end
  end
  local t = {}
  for k, _ in pairs(m) do
    t[#t + 1] = k
  end
  return t
end

-- to specifi formats like a 720p|720x1280
---@param t table
function M.is_formats_mask(t)
  local f = type(t) == 'table' and #t > 0 and
      (tonumber(t[1]) == nil or tonumber(t[2]) == nil)
  log_debug("is_formats_mask a:%s v:%s ret:%s", (t or E)[1], (t or E)[2], f)
  return f
end

---@param min_bitrate number 48000
function M.find_itag_with_audio_bitrate_more_than(af, min_bitrate)
  assert(type(min_bitrate) == 'number', 'min_bitrate')
  for _, f in pairs(af) do
    if f.averageBitrate and f.averageBitrate > min_bitrate then
      return f.itag
    end
  end
end

---@param min_height number
---@param min_width number
function M.find_itag_with_resolution_more_than(vf, min_height, min_width)
  if type(vf) then
    for _, f in pairs(vf) do
      local h = tonumber(f.height)
      local w = tonumber(f.width)
      if (h and w) and (h >= min_height or w >= min_width) then
        return f.itag
      end
    end
  end
  return nil
end

---@param s string
function M.video_format_mask_to_resolution(s)
  local sres = M.RESOLUTIONS[s or false] or s
  if type(sres) == 'string' then
    local h, w = string.match(sres, '^(%d+)x(%d+)$')
    return tonumber(h) or M.DEF_HEIGTH, tonumber(w) or M.DEF_WIDTH
  end
  return M.DEF_HEIGTH, M.DEF_WIDTH
end

---@param formats table
---@return table
function M.resolve_format_masks(formats, af, vf)
  log_debug("resolve_format_masks f:%s af:%s vf:%s", formats, #(af or E), #(vf or E))
  local t = {}
  local audiof = formats[1]
  local videof = formats[2]
  local abitrate

  if (audiof or ''):sub(1, 1) == 'a' then
    abitrate = tonumber(string.match(audiof, 'a(%d+)k'))
    if abitrate then
      abitrate = abitrate * 1000
    else
      abitrate = M.DEF_AUDIO_BITRATE
    end
    t[1] = M.find_itag_with_audio_bitrate_more_than(af, abitrate)
  elseif tonumber(audiof) ~= nil then
    t[1] = tonumber(audiof)
  else
    t[1] = audiof
  end

  if videof ~= nil and tonumber(videof) == nil then -- 720p or 720x1280
    local minh, minw = M.video_format_mask_to_resolution(videof)
    t[2] = M.find_itag_with_resolution_more_than(vf, minh, minw)
  elseif tonumber(videof) ~= nil then
    t[2] = tonumber(videof)
  else
    t[2] = videof
  end

  log.debug('resolve_format_mask from a:"%s" v:"%s" to "%s" (abr:%s)',
    audiof, videof, t, abitrate)
  return t
end

---@param vid string
function M.fetch_formats_from_page(vid)
  local ok, json = ytapi.get_video_info(vid)
  if not ok then return false, 'video_info:' .. v2s(json) end ---@cast json string

  local data = uhttp.decode_json(json)
  if not data then
    return false, 'cannot parse json fro item: ' .. v2s(vid)
  end

  -- if opts.mime_types_only then
  --   return false, M.get_all_mime_types(data)
  -- end

  local af, vf = M.get_formats_vid(data)
  return af, vf
end

---@param vid string
---@return false|table?
---@return string|table?
---@param opts table
function M.fetch_formats_via_yt_dlp(vid, opts)
  log_debug('fetch formats via yt-dlp', vid)
  local formats_table, errmsg = yt_dlp.fetch_formats(vid, opts)
  if not formats_table then
    return false, errmsg
  end

  local af, vf = yt_dlp.get_formats_vid(formats_table)
  return af, vf
end

---@param params table
---@param opts table?
---@return table? af
---@return table? vf
---@return string? err
function M.prepare_vid_formats(params, opts)
  assert(type(params) == 'table', 't')
  assert(type(params.vid) == 'string', 't.vid')
  local vid = params.vid
  opts = opts or E
  log_debug("prepare_vid_formats %s via %s", vid, formats_extractor)

  local af, vf
  if formats_extractor == YT_DLP then
    af, vf = M.fetch_formats_via_yt_dlp(vid, opts)
    if type(af) ~= 'table' or type(vf) ~= 'table' then
      return nil, nil, 'on fetch formats via yt-dlp error:' .. v2s(vf)
    end
  else
    af, vf = M.fetch_formats_from_page(vid)
    if type(af) ~= 'table' or type(vf) ~= 'table' then
      return nil, nil, 'cannot fetch formats from page vid: ' .. v2s(vid)
    end
  end

  if not opts.no_sort then
    -- sort by ascending bitrate
    table.sort(af or {}, M.comp_format_entries_asc)
    table.sort(vf or {}, M.comp_format_entries_asc)
  end

  -- if opts.mime_types_only then
  --   return false, M.get_all_mime_types(data)
  -- end

  return af, vf, nil
end

--
-- download audio and video parts defined by given formats
-- formats[1] is for audio, format[2] for video
--
---@param params table{vid:string}
---@param formats table{[1], [2]}
function M.download_vid(params, formats, opts)
  log_debug("download_vid formats:%s opts:%s", formats, opts)
  local af, vf, err = M.prepare_vid_formats(params, opts)
  if err then
    return false, err
  end
  -- resolve format mask to itag number. e.g. [a 720p] ->
  if M.is_formats_mask(formats) then
    formats = M.resolve_format_masks(formats, af, vf)
  end

  -- to build size report without downloading
  if opts.only_size then
    if #(formats or E) == 0 then
      return false, 'you should define default formats'
    end
    return M.fetch_format_size_for_vid(formats, af, vf, opts)
  end

  -- ask the user
  if not formats or #formats == 0 then
    log.debug('prompt user for format interactively')
    print(M.fmt_formats(af, vf))

    if opts.ask then
      local a0 = M.find_itag_with_audio_bitrate_more_than(af, 42000)
      local v0 = M.find_itag_with_resolution_more_than(vf, 720, 1280)
      local def = v2s(a0) .. ' ' .. v2s(v0)
      local val = base.ask_value('formats[' .. def .. ']:', def)
      local an, vn = string.match(val or '', '^([%w_%-]+)%s+([%w_%-]+)$')
      if an and vn then
        formats = { an, vn }
      else
        -- only audio
        an = string.match(val or '', '^%s*([%w_%-]+)%s*$') -- 139-dash
        if an then
          formats = { an }
        else
          vn = string.match(val or '', '^na%s+([%w_%-]+)%s*$')
          formats = { false, vn }
        end
      end
      log.debug('user define formats: %s defaults:', formats, def)
    end

    if #(formats or E) == 0 then
      return false, 'no formats. use: --formats [a48k 720p]'
    end
  end

  if af and vf then
    -- urls "application/vnd.yt-ump"
    if opts.show_urls then
      local urls = M.get_format_urls(af, vf, formats)
      for _, url in ipairs(urls) do print(url) end
      return
    end
    return M.fetch_format_files_for_vid(params, formats, af, vf, opts.dry_run)
  else
    return false, 'af:' .. v2s(af ~= nil) .. ' vf:' .. v2s(vf ~= nil)
  end
end

---@param plid string
---@param formats table
---@param items table
function M.download_playlist(plid, formats, items, opts)
  if not items or #items == 0 then
    return false, 'no items. use --items [1-10]'
  end
  if (opts or E).only_size and #(formats or E) == 0 then
    return false, "you must define default formats"
  end
  log.debug('download_playlist %s formats:%s items:%s', plid, formats, items)
  local ok, body = ytapi.get_playlist_info(plid)
  if not ok or not body then return false, body end

  local ok2, ret = ytapi.get_playlist_content(body)
  if not ok2 then return false, v2s(ret) end ---@cast ret table

  local flat_indexes = M.parse_items(items)
  local map_indexes = base.tbl_list_to_map(flat_indexes, true)
  opts = opts or {}
  local dirs = opts.skip_already_downloaded
  local ignore_vid, cnt = nil, 0

  if type(dirs) == 'table' and #dirs > 0 then
    ignore_vid, cnt = M.build_ignore_vid(dirs, {})
    print(fmt("Already downloaded[%s]:\n %s\n", cnt,
      table.concat(base.tbl_keys(ignore_vid), " ")))
  end

  for _, e in ipairs(ret) do
    local vid = assert(e.vid, 'videoId')
    local index = assert(e.index, 'index')
    local skip = not map_indexes[tonumber(index)] or (ignore_vid or E)[vid]
    local rskip = (skip and 'skip' or 'download')
    print(fmt("%3s  %s  %s %s", v2s(index), v2s(vid), rskip, v2s(e.title)))

    if not skip then
      opts.ask = true
      local ok_call, ok3, ret3 = pcall(M.download_vid, e, formats, opts)
      log_debug("download_vid say ok:%s ret:%s", ok3, ret3)
      if not ok_call then
        print('error on downlod vid:' .. v2s(vid) .. " " .. v2s(ok3))
      elseif not ok3 and not (opts or E).quiet then
        print(ret3)
      end
    end
  end
  return true
end

--
-- extract vid from filename
---@param fn string
function M.get_vid_from_filename(fn)
  return fn ~= nil and string.match(fn, "%-(...........)%.[%w]+$") or nil
end

-- build the map with vid what already exists in given directories
---@param dirs table
---@param out table
function M.build_ignore_vid(dirs, out)
  out = out or {}
  local opts = { ftype = 'file', basenames = true }
  local filter = nil --"%.mp4$"
  local c = 0
  for _, dir in ipairs(dirs) do
    if base.dir_exists(dir) then
      local files = base.list_of_files(dir, filter, opts)
      if files and #files > 0 then
        for _, fn in ipairs(files) do
          local vid = M.get_vid_from_filename(fn)
          if vid and not out[vid] then
            out[vid] = true -- mark as downloaded
            c = c + 1
            -- check to filesize?
          end
        end
      end
      -- instead of dir name given vid itself
    elseif #dir == ytapi.VID_LEN then
      out[dir] = true
    end
  end
  return out, c
end

return M
