-- 26-07-2024 @author Swarg
--
-- TODO: configurable --extractor-args "youtube:player_client=android,web,ios"
-- android,web,ios is the default.
--
local log = require 'alogger'
local base = require 'cli-app-base'
local settings = require 'ytcli.settings'
local cjson = require 'cjson'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, match = {}, tostring, string.format, string.match
local log_debug = log.debug
local execr = base.execr

local null = cjson.null

local M = {}

-- fromat types
local AUDIO_ONLY = 'audio_only'
local VIDEO_ONLY = 'video_only'
local AUDIO_VIDEO = 'audio+video'
local STORYBOARD = "storyboard" -- ?
local BROKEN = "BROKEN"         -- ?



---@param proxy string?
function M.mk_proxy_opts(proxy)
  -- 10.0.0.1:8080
  proxy = proxy or (settings.config or E).proxy

  if type(proxy) == 'string' and proxy ~= '' and proxy ~= 'no' then
    return '--no-check-certificate --proxy ' .. v2s(proxy)
  end

  return '' -- no proxy
end

---@param vid string
---@param opts table?
function M.fetch_formats(vid, opts)
  log_debug("fetch_formats opts:%s", opts)
  assert(type(vid) == 'string' and vid ~= '', 'vid')
  opts = opts or {}
  -- use `--` to signify the end of the options so that any input that follows
  -- will be interpreted as URLs e.g. `yt-dlp -- -QxK8NvFbaU`
  -- --extractor-args "youtube:player_client=android,web,ios"

  local eo = ''
  if opts.extractor_args then
    eo = eo .. ' --extractor-args "' .. v2s(opts.extractor_args) .. '"'
  elseif settings.get_extractor_args() then
    eo = eo .. ' --extractor-args "' .. v2s(settings.get_extractor_args()) .. '"'
  end
  eo = eo .. M.mk_proxy_opts(opts.proxy)

  -- old way via formats table
  -- local cmd = 'yt-dlp --no-colors ' .. eo .. ' -F --print-json -- ' .. vid
  -- return M.parse_formats_old(base.execr(cmd, opts.dry_run, opts.verbose))

  -- new way via, another way is big json output --dump-json(all)
  local cmd = 'yt-dlp  --print "%(formats)#j" ' .. eo .. ' -- ' .. vid

  return M.parse_formats_from_json(execr(cmd, opts.dry_run, opts.verbose), vid)
end

---@param v any
---@return boolean
local function is_num(v)
  return type(v) == 'number'
end

-- audio_only | video_only | audio+video | ...
function M.get_format_type(t)
  assert(type(t) == 'table', 't')
  local abr = is_num(t.ABR) and t.ABR or 0 -- audio bitrate
  local vbr = is_num(t.VBR) and t.VBR or 0 -- video bitrate

  if vbr > 0 and abr > 0 then
    return AUDIO_VIDEO
  elseif vbr > 0 and abr <= 0 then --is_num(t.FPS) and t.FPS > 1 then
    return VIDEO_ONLY
  elseif abr > 0 and vbr == 0 then
    return AUDIO_ONLY
  elseif abr == 0 and vbr == 0 and t.format_note == 'storyboard' then
    return STORYBOARD
  elseif t.ABR == nil and t.VBR == nil then
    return BROKEN
  elseif t.PROTO == "m3u8_native" then
    return nil -- skip
  else
    error('[DEV] unexpected format type' .. require 'inspect' (t))
    return nil
  end
end

---@param t table
---@return table?, table?
function M.get_formats_vid(t)
  assert(type(t) == 'table' and type(t.formats) == 'table', 'expected formats')

  local vf, af = {}, {}
  for _, e in ipairs(t.formats) do
    local typ = M.get_format_type(e)
    if typ == AUDIO_ONLY then
      af[#af + 1] = M.mk_format_entry(e, AUDIO_ONLY)
    elseif typ == VIDEO_ONLY then
      vf[#vf + 1] = M.mk_format_entry(e, VIDEO_ONLY)
    end
  end
  return af, vf
end

local function bitrate_to_num(s)
  if type(s) == 'string' then
    if s:sub(-1, -1) == 'k' then
      s = s:sub(1, -2)
    end
    local n = tonumber(s)
    if n then
      return n * 1000
    end
  elseif type(s) == 'number' then
    return math.ceil(s * 1000)
  end
  return nil
end


---@param e table{ID EXT RESOLUTION FPS CH ..}
function M.mk_format_entry(e, mtype, subtype, codecs)
  local ab, width, height
  subtype = subtype or e.EXT
  mtype = mtype or M.get_format_type(e)

  if mtype == VIDEO_ONLY then
    ab = bitrate_to_num(e.VBR)
    width, height = e.width, e.height
    if (not width or not height) and e.RESOLUTION then
      width, height = string.match(e.RESOLUTION or '', "^(%d+)x(%d+)$")
    end
    codecs = codecs or e.VCODEC
  elseif mtype == AUDIO_ONLY then
    ab = bitrate_to_num(e.ABR)
    codecs = codecs or e.ACODEC
  elseif mtype == AUDIO_VIDEO then
    ab = (bitrate_to_num(e.ABR) or 0) + (bitrate_to_num(e.VBR) or 0)
    codecs = codecs or e.VCODEC
    --
  elseif mtype == BROKEN or mtype == STORYBOARD then
    return nil -- ignore
  else
    error('unknown mtype:' .. v2s(mtype))
  end

  if not ab then
    error('Error no averageBitrate: ' .. require "inspect" (e) .. ' ' .. v2s(mtype))
  end

  return {
    mime = { mt = mtype, subt = subtype, codecs = codecs },
    itag = e.ID, -- itag,
    ext = e.EXT,
    filesize = e.FILESIZE,
    -- url = e.url,
    -- lastModified = e.lastModified,
    -- contentLength = e.contentLength,
    averageBitrate = ab,
    -- approxDurationMs = e.approxDurationMs,
    -- qualityLabel = e.qualityLabel,
    -- quality = e.quality,
    height = tonumber(height),
    width = tonumber(width),
    fps = e.FPS, -- fps,
    url = e.url,
  }
end

-- convert cjson.null into lua nil
local function cjson2lua(e)
  if e == null or e == 'none' then
    return nil
  end
  if type(e) == 'userdata' then
    return tostring(e)
  end
  return e
end

-- Parse output lines from system command `yt-dlp --no-colors -F --print-json <VideoID>`
-- into table
---@return false|table{vid = '?', formats:table}
---@return string?
function M.parse_formats_from_json(s, vid)
  log_debug("parse_formats_from_json", #(s or ''))

  if settings.is_dump_formats() then
    base.write('./' .. v2s(vid) .. '_formats.json', s)
  end

  local ok, json_list = pcall(cjson.decode, s)
  if not ok then
    return false, v2s(json_list) -- errmsg
  end

  local t = {}
  local j2l = cjson2lua
  for _, e in ipairs(json_list) do
    local id = e.format_id
    if id then
      t[#t + 1] = {
        ID = e.format_id,
        EXT = e.ext,
        RESOLUTION = j2l(e.resolution), -- "audio only", "1280x720"
        height = j2l(e.height),
        width = j2l(e.width),
        FPS = j2l(e.fps),
        CH = j2l(e.audio_channels),
        FILESIZE = j2l(e.filesize) or j2l(e.filesize_approx),
        format_note = e.format_note, -- 720p
        ACODEC = j2l(e.acodec),
        VCODEC = j2l(e.vcodec),
        ABR = j2l(e.abr),
        VBR = j2l(e.vbr),
        ASR = j2l(e.asr),
        PROTO = j2l(e.protocol),
        url = j2l(e.url), -- direct url
        -- ext, audio_ext, video_ext
        -- container
      }
    end
  end
  return { vid = vid, formats = t }
end

-- helper
---@param t table{vid:string, formats:table}
---@return table?
---@param itag string
function M.get_itag(t, itag)
  for _, e in pairs(t.formats) do
    if e.ID == itag then
      return e
    end
  end
  return nil
end

M.testing = {
  cjson2lua = cjson2lua,
  bitrate_to_num = bitrate_to_num,
}

return M
