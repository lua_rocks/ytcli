-- 22-02-2024 @author Swarg
--
-- CLI Command Handler
-- Entry point of a cli application
--

local log = require('alogger')
local Cmd4Lua = require('cmd4lua')
local builtin = require "cli-app-base.builtin-cmds"

local yt = require "ytcli.cmd.yt"
local discord = require "ytcli.cmd.discord"

local M = {}
M._VERSION = 'ytcli v0.11.1'
M._URL = 'https://gitlab.com/lua_rocks/ytcli'


---@param args string|table
function M.handle(args)
  log.debug('handle', args)
  return
      Cmd4Lua.of(args)
      :root('ytcli')
      :about('The ToolSet for yt ' .. tostring(M._VERSION))
      :handlers(M)

      :desc('version of this application')
      :cmd('version', 'v')

      --  the builtin commands

      :desc('built-in commands of the appliction, such as config, logger')
      :cmd('builtin', 'b', builtin.handle)

      --  the domain commands:

      :desc('interact with api key')
      :cmd('api-key', 'ak', yt.cmd_api_key)

      :desc('get video info by id')
      :cmd('video-info', 'vi', yt.cmd_video_info)

      :desc('get videos list for given channel')
      :cmd('videos-list', 'vl', yt.cmd_videos_list)

      :cmd('channel-info', 'ci', yt.cmd_channel_info)

      :cmd('playlist-info', 'pi', yt.cmd_playlist_info)

      :desc('get subtitles for given video-id')
      :cmd('subtitles', 's', yt.cmd_subtitles)

      :desc('subscriptions to content and notifications')
      :cmd('subscriptions', 'n', yt.cmd_subscriptions)

      :desc('download one or multiples media files')
      :cmd('download', 'd', yt.cmd_download)

      :desc('install dependency to download media files')
      :cmd('install-yt-dlp', 'iyd', yt.cmd_install_yt_dlp)

      -- debugging

      :desc('uncompress given file')
      :cmd('ungzip', 'u', yt.cmd_ungzip)

      :desc('clear inner cache of video json data')
      :cmd('clear-cache', 'cc', yt.cmd_clear_cache)

      :desc('interact with discord channels via webhooks')
      :cmd('discord', 'D', discord.handler)

      :run()
      :exitcode()
end

--
-- version of this application
--
function M.cmd_version()
  io.stdout:write(M._VERSION .. "\n")
end

return M
