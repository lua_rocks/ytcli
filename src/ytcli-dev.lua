#!/usr/bin/env lua
--
-- 22-02-2024 @author Swarg
--
-- Goal: cli tool for yt
--
-- Dependencies:
--   Luarocks:
--      alogger cmd4lua inspect cjson
--   System:
--     readlink
--
-- This script file was used as the entry point to this program,
-- through manual creation of a symbolic link to this file located in PATH:
--
-- To install this script as executable use:
--   ln -s ${PWD}/src/yt-tool-dev.lua ${HOME}/.local/bin/yt-tool
--
-- If the program is installed via luarocks as rock package, then
-- this script is not needed and `bin/yt-tool` is used instead
--
-- This script file is intended for dev environments, allowing you to write
-- an executable command directly to the source files and
-- see the changes in the behavior of the program without reinstalling the rock
-- use for this command:
--
--   make symlink
--

--
-- helper to get a real full path to the dir of the current script file
-- used to run app not from the project_root (i.g. via soft link from PATH)
--
local function get_app_dir()
  local file = io.popen('readlink -f ' .. tostring(arg[0]), 'r')
  if file then
    local output = file:read('*all')
    file:close()
    -- dir only without current filename
    return output:match("(.*[/\\])") or output
  end
  return 'src'
end

-- Global (stored in a _G.APP)
APP = { appdir = get_app_dir() }

package.path = package.path .. ";" .. APP.appdir .. '?.lua'

local handler = require('ytcli.handler')
local settings = require('ytcli.settings')

--
--
local function main(arg)

  settings.setup()
  -- require 'alogger'.fast_setup()
  return handler.handle(arg) -- exit code
end

os.exit(main(arg))
