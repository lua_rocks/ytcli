## YT-CLI

CLI tool for interact with video-services.

## Features

- produce readable info about video by it video-id.
- download subtitles for given video or for all videos in current directory.
- track yt playlist updates to new videos and send notification to configured
  discord channel(via webhook).




## Installation

install latest version via luarocks
```sh
luarocks install https://gitlab.com/lua_rocks/ytcli/-/raw/master/ytcli-0.4.0-1.rockspec?ref_type=heads
```

Variant two
```sh
git clone --depth 1 https://gitlab.com/lua_rocks/ytcli
cd ytcli
luarocks make
ytcli version
```

Variant three: installation ytcli executable for `development` environment
ensure you have exited ~/.local/bin directory defined in your $PATH
(install for current user only(into ~/.local/bin) without luarocks)
```sh
git clone --depth 1 https://gitlab.com/lua_rocks/ytcli
make link
```


## Usage Examples:

- config file
```sh
ytcli builtin config path
# or
ytcli b c p

/home/user/.config/ytcli/config.lua
```

- add webhook to send notification to discord channel
(to get webhook url go to the settings of the channel in integration sections)
```sh
ytcli discord add <WebHookName> 'https://discord.com/api/webhooks/<ID>/<TOKEN>'
```

check configured webhooks:
```sh
ytcli discord list
# Output:
# <WebHook-Name> <WebHookId>
ytcli discord send-message <WebHookName> [ the message to the discrod channel ]
```

add subscription to the playlist
```sh
ytcli subscriptions add $PlayList_ID
# or
ytcli n a $PlayList_ID

# show configured subscriptions ids
ytcli subscriptions list
```

track news in subscriptions with notification via discord webhook (infinity loop)
```sh
ytcli subscriptions track --discord-channel <WebHookName> --delay 60 --verbose
```

debugging

```sh
YTCLI_DEBUG=1 ytcli vi <video-id>
```

> Downloading

download one video by given VideID (hashcode-like string with 11 characters)
```sh
ytcli download <VideoID> -f def
```

Find out the size of files for downloading from a specified playlist
```sh
ytcli download <PLAYLIST_ID> -i 1-99 --only-size -f def
```
- `-i 1-99` - define items  from 1 to 99 in given playlist
- `-f def` - use default formats itags to auto select format from available
- `--only-size | -Z` - show only size of all defined formats without actual
  downloading


## Warnings

is at an early stage of development.


## TODOList

- [+] fetch all links for yt/@channel/videos

