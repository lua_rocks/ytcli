---@diagnostic disable: lowercase-global
package = "ytcli"
version = "0.11.1-1"
source = {
  url = "git://gitlab.com/lua_rocks/ytcli.git",
  tag = "v0.11.1"
}
description = {
  summary = "CLI tool for interact with video-services",
  detailed = [[
Features:
- produce readable info about video by it video-id.
- download subtitles for given video or for all videos in current directory.
- track yt playlist updates to new videos and send notification to configured discord channel(via webhook).
  ]],
  homepage = "https://gitlab.com/lua_rocks/ytcli",
  license = "MIT"
}

dependencies = {
  'lua >= 5.1',
  'alogger >= 0.2.2',
  'cmd4lua >= 0.8.0',
  'cli-app-base >= 0.7',
  'inspect >= 3.1',
  'lua-cjson >= 2.1',
  'luasocket >= 3.1',
}

build = {
  install = {
    bin = {
      ['ytcli'] = 'bin/ytcli'
    }
  },
  type = "builtin",
  modules = {
    ["ytcli-dev"] = "src/ytcli-dev.lua",
    ["ytcli.cmd.yt"] = "src/ytcli/cmd/yt.lua",
    ["ytcli.cmd.discord"] = "src/ytcli/cmd/discord.lua",
    ["ytcli.handler"] = "src/ytcli/handler.lua",
    ["ytcli.settings"] = "src/ytcli/settings.lua",
    ["ytcli.subtitles"] = "src/ytcli/subtitles.lua",
    ["ytcli.subscriptions"] = "src/ytcli/subscriptions.lua",
    ["ytcli.yt-api"] = "src/ytcli/yt-api.lua",
    ["ytcli.discord-api"] = "src/ytcli/discord-api.lua",
    ["ytcli.download"] = "src/ytcli/download.lua",
    ["ytcli.util.yt_dlp"] = "src/ytcli/util/yt_dlp.lua",
  }
}
