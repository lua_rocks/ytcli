-- 26-07-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'ytcli.util.yt_dlp'
local base = require 'cli-app-base'
local cjson = require 'cjson'


describe("ytcli.util.yt_dlp", function()
  -- fetch media formats via yt-dlp
  -- --newline
  -- --dump-pages
  -- --write-pages

  it("bitrate_to_num", function()
    local f = M.testing.bitrate_to_num
    assert.same(49000, f(49.0))
  end)

  it("cjson2lua", function()
    local s = '{"key":null}'
    local o = cjson.decode(s)
    assert.equal(cjson.null, o.key)
    assert.is_not_nil(cjson.null)
    assert.same(nil, M.testing.cjson2lua(cjson.null))
    assert.same(nil, M.testing.cjson2lua(o.key))

    assert.same(nil, M.testing.cjson2lua('none'))
  end)

  local fmt_video_only_136 = {
    ID = "136",
    EXT = "mp4",
    PROTO = "https",
    FILESIZE = 26707997,
    ABR = 0,
    VBR = 101.487,
    FPS = 15,
    RESOLUTION = "1280x720",
    height = 720,
    width = 1280,
    url = "https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=136&...",
    format_note = "720p",
  }

  local fmt_audio_only_139 = {
    ID = "139",
    EXT = "m4a",
    PROTO = "https",
    CH = 1,
    ABR = 47.671,
    ASR = 22050,
    ACODEC = "mp4a.40.5",
    FILESIZE = 12546244,
    RESOLUTION = "audio only",
    VBR = 0,
    url = "https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=139&...",
    format_note = "low",
  }

  local fmt_audio_only_251_drc_broken = {
    ID = "251-drc",
    EXT = "webm",
    PROTO = "https",
    CH = 2,
    ABR = 98.351,
    ASR = 48000,
    ACODEC = "opus",
    FILESIZE = 25883858,
    RESOLUTION = "audio only",
    VBR = 0,
    url = "https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?...",
    format_note = "medium, DRC, BROKEN",
  }
  local fmt_audio_video_18_broken = {
    ID = "18",
    EXT = "mp4",
    CH = 1,
    PROTO = "https",
    -- ABR = nil    thats why its broken?
    -- VBR = nil
    ASR = 44100,
    ACODEC = "mp4a.40.2",
    VCODEC = "avc1.42001E",
    FILESIZE = 36335765,
    FPS = 15,
    RESOLUTION = "640x360",
    height = 360,
    width = 640,
    format_note = "360p, BROKEN",
    url = "https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=18&...",
  }
  local fmt_storyboard = {
    ID = "sb2",
    EXT = "mhtml",
    FPS = 0.04750593824228,
    PROTO = "mhtml",
    RESOLUTION = "48x27",
    ABR = 0,
    VBR = 0,
    height = 27,
    width = 48,
    url = "https://i.ytimg.com/sb/VideoId/storyboard3_L0/default.jpg?sqp=..==&sigh=..",
    format_note = "storyboard",
  }


  it("get_format_type", function()
    local f = M.get_format_type
    assert.same('video_only', f(fmt_video_only_136))
    assert.same('audio_only', f(fmt_audio_only_139))
    assert.same('BROKEN', f(fmt_audio_video_18_broken))
    assert.same('storyboard', f(fmt_storyboard))
  end)

  it("mk_format_entry", function()
    local exp = {
      averageBitrate = 98351,
      ext = 'webm',
      filesize = 25883858,
      itag = '251-drc',
      mime = { codecs = 'opus', mt = 'audio_only', subt = 'webm' },
      url = 'https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?...'
    }
    assert.same(exp, M.mk_format_entry(fmt_audio_only_251_drc_broken))
  end)



  it("mk_format_entry", function()
    local exp = {
      averageBitrate = 101487,
      ext = 'mp4',
      filesize = 26707997,
      fps = 15,
      height = 720,
      itag = '136',
      mime = { mt = 'video_only', subt = 'mp4' },
      url = 'https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=136&...',
      width = 1280
    }
    assert.same(exp, M.mk_format_entry(fmt_video_only_136))
  end)

  it("mk_format_entry", function()
    assert.same(nil, M.mk_format_entry(fmt_audio_video_18_broken))
  end)




  it("parse_formats_from_json", function()
    -- if 0 == 0 then return end
    local json = base.read_all('./spec/resources/formats_uYfswrGoKZY.json')
    local formats = M.parse_formats_from_json(json)
    -- print("[DEBUG] formats:", require "inspect" (formats))
    assert.is_table(formats) ---@cast formats table

    local af, vf = M.get_formats_vid(formats)
    local exp_af = {
      {
        averageBitrate = 98351,
        ext = 'webm',
        filesize = 25883858,
        itag = '251-drc',
        mime = { codecs = 'opus', mt = 'audio_only', subt = 'webm' },
        url = 'https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=251'
      },
      {
        averageBitrate = 96448,
        ext = 'webm',
        filesize = 25382969,
        itag = '251',
        mime = { codecs = 'opus', mt = 'audio_only', subt = 'webm' },
        url = 'https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=251'
      },
      {
        averageBitrate = 48784,
        ext = 'm4a',
        filesize = 12839739,
        itag = '139-drc',
        mime = { codecs = 'mp4a.40.5', mt = 'audio_only', subt = 'm4a' },
        url = 'https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=139'
      },
      {
        averageBitrate = 47671,
        ext = 'm4a',
        filesize = 12546244,
        itag = '139',
        mime = { codecs = 'mp4a.40.5', mt = 'audio_only', subt = 'm4a' },
        url = 'https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=139'
      },
      {
        averageBitrate = 129474,
        ext = 'm4a',
        filesize = 34074974,
        itag = '140-drc',
        mime = { codecs = 'mp4a.40.2', mt = 'audio_only', subt = 'm4a' },
        url = 'https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=140'
      },
      {
        averageBitrate = 95154,
        ext = 'm4a',
        filesize = 25042356,
        itag = '140',
        mime = { codecs = 'mp4a.40.2', mt = 'audio_only', subt = 'm4a' },
        url = 'https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=140'
      }
    }
    assert.same(exp_af, af)

    local exp_vf = {
      {
        itag = '269',
        averageBitrate = 173093,
        ext = 'mp4',
        fps = 15,
        height = 144,
        mime = { codecs = 'avc1.4D400B', mt = 'video_only', subt = 'mp4' },
        url = 'https://manifest.googlevideo.com/api/manifest/hls_playlist/...',
        width = 256
      },
      {
        itag = '229',
        averageBitrate = 313793,
        ext = 'mp4',
        fps = 15,
        height = 240,
        mime = { codecs = 'avc1.4D400C', mt = 'video_only', subt = 'mp4' },
        url = 'https://manifest.googlevideo.com/api/manifest/hls_playlist/...',
        width = 426
      },
      {
        itag = '230',
        averageBitrate = 219011,
        ext = 'mp4',
        fps = 15,
        height = 360,
        mime = { codecs = 'avc1.4D4016', mt = 'video_only', subt = 'mp4' },
        url = 'https://manifest.googlevideo.com/api/manifest/hls_playlist/..',
        width = 640
      },
      {
        itag = '134',
        averageBitrate = 43429,
        ext = 'mp4',
        filesize = 11429266,
        fps = 15,
        height = 360,
        mime = { codecs = 'avc1.4D4016', mt = 'video_only', subt = 'mp4' },
        url = 'https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=134&..',
        width = 640
      },
      {
        itag = '136',
        averageBitrate = 101487,
        ext = 'mp4',
        filesize = 26707997,
        fps = 15,
        height = 720,
        mime = { codecs = 'avc1.4D401F', mt = 'video_only', subt = 'mp4' },
        url = 'https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=136',
        width = 1280
      }
    }
    assert.same(exp_vf, vf)
  end)

  local fmt_yt_dlp_136_approx = [[
    [{
        "asr": null,
        "filesize": 26707997,
        "format_id": "136",
        "format_note": "720p",
        "source_preference": -1,
        "fps": 15,
        "audio_channels": null,
        "height": 720,
        "quality": 8.0,
        "has_drm": false,
        "tbr": 101.487,
        "filesize_approx": 26707991,
        "url": "https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=136",
        "width": 1280,
        "language": null,
        "language_preference": -1,
        "preference": null,
        "ext": "mp4",
        "vcodec": "avc1.4D401F",
        "acodec": "none",
        "dynamic_range": "SDR",
        "container": "mp4_dash",
        "downloader_options": { "http_chunk_size": 10485760 },
        "protocol": "https",
        "resolution": "1280x720",
        "aspect_ratio": 1.78,
        "video_ext": "mp4",
        "audio_ext": "none",
        "abr": 0,
        "vbr": 101.487,
        "format": "136 - 1280x720 (720p)"
    }]
    ]]

  it("parse_formats_from_json filesize_approx", function()
    local f = M.parse_formats_from_json
    local exp = {
      formats = {
        {
          ID = '136',
          ABR = 0,
          EXT = 'mp4',
          FILESIZE = 26707997,
          FPS = 15,
          PROTO = 'https',
          RESOLUTION = '1280x720',
          VBR = 101.487,
          VCODEC = 'avc1.4D401F',
          format_note = '720p',
          height = 720,
          url = 'https://rr1---sn-coct-hn9e.googlevideo.com/videoplayback?itag=136',
          width = 1280
        }
      }
    }
    assert.same(exp, f(fmt_yt_dlp_136_approx))
  end)
end)
