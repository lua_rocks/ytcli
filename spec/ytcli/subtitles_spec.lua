-- 23-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("ytcli.subtitles");

describe("ytcli.subtitles", function()
  it("fmt_sec", function()
    assert.same('00:00:22.800', M.fmt_sec(22.8))
    assert.same('00:00:22.123', M.fmt_sec(22.123))
    assert.same('00:00:22.000', M.fmt_sec(22.0))
    assert.same('01:00:00.000', M.fmt_sec(3600.0))
    assert.same('01:00:00.001', M.fmt_sec(3600.001))
    assert.same('01:01:01.900', M.fmt_sec(3661.9))
  end)

  it("xml_unescape", function()
    local line = [[What&#39;s up?]]
    assert.same("What's up?", M.xml_unescape(line))
  end)

  it("convert_transcription_from_xml", function()
    local xml0 = [[
<?xml version="1.0" encoding="utf-8" ?>
<transcript>
<text start="22.8" dur="1.94">
What&amp;#39;s up?
Welcome to Backend Stuff.
</text>
<text start="1037.32" dur="1.48">
See you next time, guys. Bye.
</text></transcript>
]]

    local exp = [[
WEBVTT

00:00:22.800 --> 00:00:24.740
What's up?
Welcome to Backend Stuff.

00:17:17.319 --> 00:17:18.799
See you next time, guys. Bye.

]]
    assert.same(exp, M.convert_xml_subs_to_vtt(xml0))
  end)


end)
