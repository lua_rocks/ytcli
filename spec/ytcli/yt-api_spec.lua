-- 22-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("ytcli.yt-api");

describe("ytcli.yt-api", function()
  it("get_vid_from_filename", function()
    local f = M.get_vid_from_filename
    assert.same('xaxxxxxxxxz', f('title-xaxxxxxxxxz'))
    assert.same('xaxxxxxxxxz', f('title-xaxxxxxxxxz_c32_ab48k'))
  end)

  local playlist_v3_info = [[
{
  "kind": "youtube#playlistListResponse",
  "etag": "Sw_XXXXXXXXXXXXXXXXXXXXXXXX",
  "pageInfo": {
    "totalResults": 1,
    "resultsPerPage": 5
  },
  "items": [
    {
      "kind": "youtube#playlist",
      "etag": "XXXXXXXXXXXXXXXXXXXXXXXXXXX",
      "id": "PLxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
      "snippet": {
        "publishedAt": "2024-02-24T18:00:00Z",
        "channelId": "UCxxxxxxxxxxxxxxxxxxxxxx",
        "title": "the title of the playlist",
        "description": "",
        "thumbnails": {
          "default": {
            "url": "https://i.ytimg.com/vi/XxxXxxxxXxX/default.jpg",
            "width": 120, "height": 90
          },
          "medium": {
            "url": "https://i.ytimg.com/vi/XxxXxxxxXxX/mqdefault.jpg",
            "width": 320, "height": 180
          },
          "high": {
            "url": "https://i.ytimg.com/vi/XxxXxxxxXxX/hqdefault.jpg",
            "width": 480, "height": 360
          },
          "standard": {
            "url": "https://i.ytimg.com/vi/XxxXxxxxXxX/sddefault.jpg",
            "width": 640, "height": 480
          },
          "maxres": {
            "url": "https://i.ytimg.com/vi/XxxXxxxxXxX/maxresdefault.jpg",
            "width": 1280, "height": 720
          }
        },
        "channelTitle": "Channel Title",
        "localized": {
          "title": "the title of the playlist",
          "description": ""
        }
      },
      "contentDetails": {
        "itemCount": 16
      }
    }
  ]
}
]]

  local apikey = "AIzaSyDCxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
  it("subscription_check_playlist", function()
    local f = M.subscription_check_playlist
    local plid = 'PLxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    local exp = {
      title = 'the title of the playlist',
      channel = 'UCxxxxxxxxxxxxxxxxxxxxxx',
      videos = 16,
    }
    assert.same(exp, f(apikey, plid, playlist_v3_info))
  end)

  it("open_node", function()
    local t = {
      a = { b = { c = { d = { "value" } } } }
    }
    assert.same({ d = { 'value' } }, M.open_node(t, { "a", "b", "c" }))
    assert.same({ 'value' }, M.open_node(t, { "a", "b", "c", "d" }))
  end)

  it("parse_line_with_vid", function()
    local f = M.parse_line_with_vid
    assert.same(nil, f("xxx"))
    assert.same('123456789AB', f("123456789AB"))
    assert.same('123456789ABX', f("123456789ABX"))
    assert.same('123456789AB', f("/watch?v=123456789AB"))
  end)

  -- abcdef12345 -> abcdef12345
  -- https://www.youtube.com/watch?v=abcdef12345   -->   abcdef12345
  it("pick_id", function()
    local f = M.extract_id_from_url_or_as_is
    assert.same('xx', f("xx"))
    assert.same('abcdef12345', f("abcdef12345"))
    assert.same('abcdef12345', f("https://www.youtube.com/watch?v=abcdef12345"))
  end)
end)
