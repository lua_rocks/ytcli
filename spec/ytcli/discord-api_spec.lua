-- 24-02-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require("ytcli.discord-api");

describe("ytcli.discord-api", function()
  it("parse_webhook_url", function()
    local function f(url)
      local id, token = M.parse_webhook_url(url)
      return tostring(id) .. '|' .. tostring(token)
    end

    local url0 = "https://discord.com/api/webhooks/"
    local wh_id = "1234567890123456789"
    local wh_token = "_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

    assert.same('nil|nil', f(''))
    local exp = wh_id .. '|' .. wh_token
    assert.same(exp, f(url0 .. wh_id .. '/' .. wh_token))
  end)
end)
