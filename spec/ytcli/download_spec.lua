-- 24-06-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'ytcli.download'

describe("ytcli.download", function()
  local mime_types = {
    'video/mp4; codecs="avc1.4d400c"',
    'video/mp4; codecs="avc1.4d4015"',
    'video/mp4; codecs="avc1.4d401f"',
    'video/mp4; codecs="avc1.4d401e"',
    'video/mp4; codecs="avc1.640028"',
    'video/mp4; codecs="avc1.64001f"',
    'audio/mp4; codecs="mp4a.40.2"',
    'audio/webm; codecs="opus"',
    'video/webm; codecs="vp9"',
  }

  it("parse_mine_type", function()
    local f = M.parse_mine_type
    local mt = mime_types
    assert.same({ 'video', 'mp4', 'codecs="avc1.4d400c"' }, { f(mt[1]) })
    assert.same({ 'video', 'mp4', 'codecs="avc1.4d4015"' }, { f(mt[2]) })
    assert.same({ 'video', 'mp4', 'codecs="avc1.4d401f"' }, { f(mt[3]) })
    assert.same({ 'video', 'mp4', 'codecs="avc1.4d401e"' }, { f(mt[4]) })
    assert.same({ 'video', 'mp4', 'codecs="avc1.640028"' }, { f(mt[5]) })
    assert.same({ 'video', 'mp4', 'codecs="avc1.64001f"' }, { f(mt[6]) })
    assert.same({ 'audio', 'mp4', 'codecs="mp4a.40.2"' }, { f(mt[7]) })
    assert.same({ 'audio', 'webm', 'codecs="opus"' }, { f(mt[8]) })
    assert.same({ 'video', 'webm', 'codecs="vp9"' }, { f(mt[9]) })
  end)

  it("comp_format_entries_asc", function()
    local e1 = { averageBitrate = 1 }
    local e2 = { averageBitrate = 2 }
    local f = M.comp_format_entries_asc
    assert.same(true, f(e1, e2))
    assert.same(false, f(e2, e1))
    local t = { e2, e1 }
    table.sort(t, M.comp_format_entries_asc)
    local exp = { { averageBitrate = 1 }, { averageBitrate = 2 } }
    assert.same(exp, t)
  end)

  it("parse_items", function()
    local f = M.parse_items
    assert.same({ 1, 2, 3, 4 }, f({ '1-4' }))
    assert.same({ 5, 6, 7, 4 }, f({ '5-7', '4' }))
    assert.same({ 1, 2, 3, 4, 5, 6, 7 }, f({ '5-7', '4', '1-3' }))
  end)

  it("find_itag_with_resolution_more_than", function()
    local f = M.find_itag_with_resolution_more_than
    local e1 = {
      itag = 136,
      height = 720,
      width = 1280,
    }
    local e2 = {
      itag = 134,
      height = 360,
      width = 640,
    }
    assert.same(136, f({ e1, e2 }, 720, 1280))
    assert.same(136, f({ e1, e2 }, 360, 640))
  end)

  it("find_itag_with_audio_bitrate_more_than", function()
    local f = M.find_itag_with_audio_bitrate_more_than
    local t = {
      {
        itag = '123',
        ext = 'm4a',
        mime = { codecs = 'mp4a.40.5', mt = 'audio', subt = 'm4a' },
        averageBitrate = -1,
      },
      {
        itag = '139-drc',
        ext = 'm4a',
        mime = { codecs = 'mp4a.40.5', mt = 'audio', subt = 'm4a' },
        averageBitrate = 49000,
      }
    }
    assert.same('139-drc', f(t, 48000))
  end)

  it("video_format_mask_to_resolution", function()
    local f = M.video_format_mask_to_resolution
    assert.same({ 720, 1280 }, { f('720p') })
    assert.same({ 144, 256 }, { f('144p') })
    assert.same({ 720, 1280 }, { f('720x1280') })
  end)

  it("resolve_format_masks", function()
    local f = M.resolve_format_masks
    local af = { {
      itag = 139,
      averageBitrate = 48000,
    } }
    local vf = { {
      itag = 136,
      height = 720,
      width = 1280,
    } }
    assert.same({ 139, 136 }, f({ 'a', '720p' }, af, vf))
  end)

  it("normalize_to_filename", function()
    local f = M.normalize_to_filename
    assert.same('ab__cd', f('ab | cd'))
    assert.same('ab_10_cd', f('ab [10] cd'))
    assert.same('ab_10_cd', f('ab (10) cd'))
    assert.same('ab_cd', f('ab; cd'))
    assert.same('ab_cd', f('ab, cd'))
    assert.same('ab_8_cd', f('ab, «8» cd'))
  end)

  it("get_vid_from_filename", function()
    local f = M.get_vid_from_filename
    local fn = "01-Public-mentoring._Introduction-abcdeMIifpU.m4a"
    assert.same('abcdeMIifpU', f(fn))

    assert.is_nil(f("01-Public-mentoring._Introduction-abcdeMIifpU.m4a.part"))
  end)

  -- 247 -> 136 when 247 give error
  it("find_similar_format", function()
    local video_formats = { {
      itag = "243",
      averageBitrate = 103965,
      ext = "webm",
      filesize = 14948932,
      fps = 30,
      height = 360,
      width = 554,
      mime = { codecs = "vp9", mt = "video_only", subt = "webm" },
    }, {
      itag = "134",
      averageBitrate = 131084,
      ext = "mp4",
      filesize = 18848243,
      fps = 30,
      height = 360,
      mime = { codecs = "avc1.4d401e", mt = "video_only", subt = "mp4" },
      width = 554
    }, {
      itag = "244",
      averageBitrate = 166030,
      ext = "webm",
      filesize = 23873049,
      fps = 30,
      height = 480,
      mime = { codecs = "vp9", mt = "video_only", subt = "webm" },
      width = 740
    }, {
      itag = "247",
      averageBitrate = 313093,
      ext = "webm",
      filesize = 45018860,
      fps = 30,
      height = 720,
      mime = { codecs = "vp9", mt = "video_only", subt = "webm" },
      width = 1110
    }, {
      itag = "136",
      averageBitrate = 419454,
      ext = "mp4",
      filesize = 60312256,
      fps = 30,
      height = 720,
      mime = { codecs = "avc1.64001f", mt = "video_only", subt = "mp4" },
      width = 1110
    }, {
      itag = "137",
      averageBitrate = 780853,
      ext = "mp4",
      filesize = 112276932,
      fps = 30,
      height = 1080,
      mime = { codecs = "avc1.640028", mt = "video_only", subt = "mp4" },
      width = 1664
    }
    }
    local f = {
      itag = "247",
      ext = "webm",
      filesize = 45018860,
      averageBitrate = 313093,
      mime = { codecs = "vp9", mt = "video_only", subt = "webm" },
      height = 720,
      width = 1110,
      fps = 30,
    }
    local exp = {
      itag = '136',
      ext = 'mp4',
      averageBitrate = 419454,
      filesize = 60312256,
      mime = { codecs = 'avc1.64001f', mt = 'video_only', subt = 'mp4' },
      height = 720,
      width = 1110,
      fps = 30,
    }
    assert.same(exp, M.find_similar_format(f, {}, video_formats))
  end)

  it("end", function()
    -- ommited fields: ext, filesize, url,
    local audio_formats = {
      {
        itag = "599-drc",
        averageBitrate = 30790,
        mime = { codecs = "mp4a.40.5", mt = "audio_only", subt = "m4a" }
      },
      {
        itag = "599",
        averageBitrate = 30790,
        mime = { codecs = "mp4a.40.5", mt = "audio_only", subt = "m4a" }
      },
      {
        itag = "600",
        averageBitrate = 32414,
        mime = { codecs = "opus", mt = "audio_only", subt = "webm" }
      },
      {
        itag = "600-drc",
        averageBitrate = 32663,
        mime = { codecs = "opus", mt = "audio_only", subt = "webm" }
      },
      {
        itag = "249-drc",
        averageBitrate = 48190,
        mime = { codecs = "opus", mt = "audio_only", subt = "webm" }
      },
      {
        itag = "139",
        averageBitrate = 48789,
        mime = { codecs = "mp4a.40.5", mt = "audio_only", subt = "m4a" }
      },
      {
        itag = "139-drc",
        averageBitrate = 48789,
        mime = { codecs = "mp4a.40.5", mt = "audio_only", subt = "m4a" }
      },
      {
        itag = "250",
        averageBitrate = 62155,
        mime = { codecs = "opus", mt = "audio_only", subt = "webm" }
      },
      {
        itag = "250-drc",
        averageBitrate = 62285,
        mime = { codecs = "opus", mt = "audio_only", subt = "webm" }
      },
      {
        itag = "251",
        averageBitrate = 111385,
        mime = { codecs = "opus", mt = "audio_only", subt = "webm" }
      },
      {
        itag = "251-drc",
        averageBitrate = 111642,
        mime = { codecs = "opus", mt = "audio_only", subt = "webm" }
      },
      {
        itag = "140",
        averageBitrate = 129475,
        mime = { codecs = "mp4a.40.2", mt = "audio_only", subt = "m4a" }
      },
      {
        itag = "140-drc",
        averageBitrate = 129475,
        mime = { codecs = "mp4a.40.2", mt = "audio_only", subt = "m4a" }
      }
    }
    local format = {
      itag = "249",
      averageBitrate = 47190,
      mime = { codecs = "opus", mt = "audio_only", subt = "webm" }
    }
    assert.same(true, M.is_format_audio_only(format))
    local exp = {
      itag = '139',
      averageBitrate = 48789,
      mime = { codecs = 'mp4a.40.5', mt = 'audio_only', subt = 'm4a' }
    }
    assert.same(exp, M.find_similar_format(format, audio_formats, {}))

    local af2 = {
      itag = "249-drc",
      averageBitrate = 48190,
      mime = { codecs = "opus", mt = "audio_only", subt = "webm" }
    }
    local exp2 = {
      averageBitrate = 48789,
      itag = '139',
      mime = { codecs = 'mp4a.40.5', mt = 'audio_only', subt = 'm4a' }
    }
    assert.same(exp2, M.find_similar_format(af2, audio_formats, {}))
  end)

  it("some_drc_fmt", function()
    local f = M.some_drc_fmt
    assert.same(true, f('249', '249-drc'))
    assert.same(false, f('139', '249-drc'))
    assert.same(true, f('249-drc', '249-drc'))
    assert.same(false, f('1-drc', '111-drc'))
    assert.same(false, f('1', '111-drc'))
    assert.same(true, f('111', '111-drc'))
    assert.same(true, f('111', '111'))
  end)
end)
