...more
<paper-ripple class="style-scope tp-yt-paper-button">
    <div id="background" class="style-scope paper-ripple"></div>
    <div id="waves" class="style-scope paper-ripple"></div>
</paper-ripple>

```js
function _handleNative$$module$third_party$javascript$polymer$v2$polymer$lib$utils$gestures(a) {
  var b = a.type,
    c = a.currentTarget.__polymerGestures;
  if (c && (c = c[b])) {
    if (!a.__polymerGesturesHandled && (a.__polymerGesturesHandled = {}, "touch" === b.slice(0, 5))) {
      var d = a.changedTouches[0];
      "touchstart" === b && 1 === a.touches.length && (sUa = d.identifier);
      if (sUa !== d.identifier) return;
      bUa || ("touchstart" === b || "touchmove" === b) && _handleTouchAction$$module$third_party$javascript$polymer$v2$polymer$lib$utils$gestures(a)
    }
    d = a.__polymerGesturesHandled;
    if (!d.skip) {
      for (var e = 0, h; e < bx.length; e++) h = bx[e], c[h.name] && !d[h.name] && h.flow && -1 < h.flow.start.indexOf(a.type) && h.reset && h.reset();
      for (e = 0; e < bx.length; e++) h = bx[e], c[h.name] && !d[h.name] && (d[h.name] = !0, h[b](a))
    }
  }
}
```
https://stackoverflow.com/questions/67615278/get-video-info-youtube-endpoint-suddenly-returning-404-not-found

curl 'https://www.youtube.com/youtubei/v1/player?key=AIzaSyAO_FJ2SlqU8Q4STEHLGCilw_Y9_11qcW8' -H 'Content-Type: application/json' --data '{  "context": {    "client": {      "hl": "en",      "clientName": "WEB",      "clientVersion": "2.20210721.00.00",      "clientFormFactor": "UNKNOWN_FORM_FACTOR",   "clientScreen": "WATCH",      "mainAppWebInfo": {        "graftUrl": "/watch?v=UF8uR6Z6KLc",           }    },    "user": {      "lockedSafetyMode": false    },    "request": {      "useSsl": true,      "internalExperimentFlags": [],      "consistencyTokenJars": []    }  },  "videoId": "UF8uR6Z6KLc",  "playbackContext": {    "contentPlaybackContext": {        "vis": 0,      "splay": false,      "autoCaptionsDefaultOn": false,      "autonavState": "STATE_NONE",      "html5Preference": "HTML5_PREF_WANTS",      "lactMilliseconds": "-1"    }  },  "racyCheckOk": false,  "contentCheckOk": false}'

