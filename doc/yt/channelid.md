## How to get channelid

 - Right click and view page source.
 - Search for "externalId", the value follows.


Alternative: run this JavaScript in console:

```js
ytInitialData.metadata.channelMetadataRenderer.externalId
```

A most simple solution for 2023 is just to get ID from meta tags:

Just run this snippet in Channel page and it will alert the id
for you to copy&paste:

```js
alert(document.querySelector('meta[property="og:url"]').getAttribute('content').split('/').at(4))
```


2024 method using yt-dlp:

You can get YouTube channel ID from channel username:

```sh
yt-dlp --print "(channel_id)s" --playlist-end 1 https://www.youtube.com/@CNN/
```

UCupvZG-5ko_eiXAupbDfxWw


Try to search for regular expression UC[-_0-9A-Za-z]{21}[AQgw] in source code.

